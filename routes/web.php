<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);

Route::get('/', ['uses' => 'EntranceController@index', 'as' => 'entrance']);

Route::get('/slide/{id}', ['uses' => 'EntranceController@showSliderNote', 'as' => 'showSliderNote']);

Route::get('/note/{id}', ['uses' => 'EntranceController@showLastNote', 'as' => 'showLastNote']);

Route::get('/products', ['uses' => 'ProductController@index', 'as' => 'catalog']);

Route::get('/products/search', ['uses' => 'ProductController@search', 'as' => 'search']);

Route::get('/products/category/{id}', ['uses' => 'ProductController@category', 'as' => 'category']);

Route::get('/products/{id}', ['uses' => 'ProductController@product', 'as' => 'product']);

Route::get('/products/addToCartFromCatalog/{id}', ['uses' => 'ProductController@addToCartFromCatalog', 'as' => 'addToCartFromCatalog']);

Route::get('/products/addToCartFromProduct/{id}', ['uses' => 'ProductController@addToCartFromProduct', 'as' => 'addToCartFromProduct']);

Route::post('/products/addReview/{id}', ['uses' => 'ProductController@addReview', 'as' => 'addReview'])->middleware('verified');

Route::get('/notes', ['uses' => 'NoteController@index', 'as' => 'notes']);

Route::get('/notes/topic/{id}', ['uses' => 'NoteController@topic', 'as' => 'topic']);

Route::get('/notes/{id}', ['uses' => 'NoteController@note', 'as' => 'note']);

Route::post('/notes/addComment/{id}', ['uses' => 'NoteController@addComment', 'as' => 'addComment'])->middleware('verified');

Route::get('/cart', ['uses' => 'ProductController@cart', 'as' => 'cart']);

Route::get('/cart/increaseProduct/{id}', ['uses' => 'ProductController@increaseProduct', 'as' => 'increaseProduct']);

Route::get('/cart/decreaseProduct/{id}', ['uses' => 'ProductController@decreaseProduct', 'as' => 'decreaseProduct']);

Route::get('/cart/deleteItems/{id}', ['uses' => 'ProductController@deleteItems', 'as' => 'deleteItems']);

Route::get('/cart/order/', ['uses' => 'ProductController@order', 'as' => 'createOrder'])->middleware('verified');

Route::get('/cart/order/registration/{id}', ['uses' => 'ProductController@orderRegistration', 'as' => 'orderRegistration'])->middleware('verified');

Route::post('/cart/ordered/registration/{id}', ['uses' => 'ProductController@ordered', 'as' => 'placeOrder'])->middleware('verified');

Route::get('/cart/order/payment/{id}', ['uses' => 'PaymentController@showPage', 'as' => 'payment'])->middleware('verified');

Route::get('/cart/order/checkout/{id}', ['uses' => 'PaymentController@showCheckout', 'as' => 'checkout'])->middleware('verified');

Route::get('/cart/order/response/{id}', ['uses' => 'PaymentController@responseCheckout', 'as' => 'response'])->middleware('verified');

Route::get('/cart/order/delete/{id}', ['uses' => 'PaymentController@deleteOrder', 'as' => 'deleteOrder'])->middleware('verified');

Route::get('/cart/order/refund/{id}', ['uses' => 'PaymentController@orderRefund', 'as' => 'refund'])->middleware('verified');

Route::get('/cart/order/return/{id}', ['uses' => 'PaymentController@returnOrder', 'as' => 'return'])->middleware('verified');

Route::get('/cart/order/response/result/succeeded', ['uses' => 'PaymentController@responseSuccess', 'as' => 'success'])->middleware('verified');

Route::get('/cart/order/response/result/canceled', ['uses' => 'PaymentController@responseCancel', 'as' => 'cancel'])->middleware('verified');

Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');

Route::get('/home/user/profile/edit/{id}', ['uses' => 'User\ProfileController@edit', 'as' => 'profileEdit'])->middleware('verified');

Route::post('/home/user/profile/edit/save/{id}', ['uses' => 'User\ProfileController@save', 'as' => 'profileSave'])->middleware('verified');

Route::get('/home/user/profile/edit/update/{id}/{token}', ['uses' => 'User\ProfileController@update', 'as' => 'profileUpdate'])->middleware('verified');

Route::get('/home/user/password/edit/{id}', ['uses' => 'User\PasswordController@edit', 'as' => 'passwordEdit'])->middleware('verified');

Route::post('/home/user/password/edit/update/{id}', ['uses' => 'User\PasswordController@update', 'as' => 'passwordUpdate'])->middleware('verified');

Route::get('/home/user/send/{id}', ['uses' => 'User\DeleteController@send', 'as' => 'userSendedDeleting'])->middleware('verified');

Route::get('/home/user/delete/{id}', ['uses' => 'User\DeleteController@delete', 'as' => 'userDelete'])->middleware('verified');

Route::get('/admin/orders', ['uses' => 'Admin\OrderController@index', 'as' => 'adminOrders'])->middleware('restrictAccess');

Route::get('/admin/orders/date/add/{id}', ['uses' => 'Admin\OrderController@add', 'as' => 'showAddOrder'])->middleware('restrictAccess');

Route::post('/admin/orders/date/added/{id}', ['uses' => 'Admin\OrderController@added', 'as' => 'addedOrder'])->middleware('restrictAccess');

Route::get('/admin/orders/date/edit/{id}', ['uses' => 'Admin\OrderController@edit', 'as' => 'showEditOrder'])->middleware('restrictAccess');

Route::post('/admin/orders/date/edited/{id}', ['uses' => 'Admin\OrderController@edited', 'as' => 'editexOrder'])->middleware('restrictAccess');

Route::get('/admin/orders/assembly/{id}', ['uses' => 'Admin\OrderController@orderAssembly', 'as' => 'orderAssembly'])->middleware('restrictAccess');

Route::get('/admin/orders/completed/{id}', ['uses' => 'Admin\OrderController@orderCompleted', 'as' => 'orderComepleted'])->middleware('restrictAccess');

Route::get('/admin/orders/refund/{id}', ['uses' => 'Admin\OrderController@orderRefund', 'as' => 'orderRefund'])->middleware('restrictAccess');

Route::get('/admin/history', ['uses' => 'Admin\OrderController@indexHistory', 'as' => 'history'])->middleware('restrictAccess');

Route::get('/admin/returns/approve/{id}', ['uses' => 'Admin\OrderController@returnApprove', 'as' => 'returnApprove'])->middleware('restrictAccess');

Route::get('/admin/returns/disapprove/{id}', ['uses' => 'Admin\OrderController@returnDisapprove', 'as' => 'returnDisapprove'])->middleware('restrictAccess');

Route::get('/admin/returns', ['uses' => 'Admin\OrderController@indexReturns', 'as' => 'returns'])->middleware('restrictAccess');

Route::get('/admin/orders/information/{id}', ['uses' => 'Admin\OrderController@orderInformation', 'as' => 'orderInformation'])->middleware('restrictAccess');

Route::get('/admin/orders/delete/{id}', ['uses' => 'Admin\OrderController@deleteOrder', 'as' => 'deleteOrder'])->middleware('restrictAccess');

Route::get('/admin/products', ['uses' => 'Admin\ProductController@index', 'as' => 'adminProducts'])->middleware('restrictAccess');

Route::get('/admin/products/add', ['uses' => 'Admin\ProductController@showAdd', 'as' => 'showAddProduct'])->middleware('restrictAccess');

Route::post('/admin/products/added', ['uses' => 'Admin\ProductController@added', 'as' => 'addedProduct'])->middleware('restrictAccess');

Route::get('/admin/products/edit/{id}', ['uses' => 'Admin\ProductController@showEdit', 'as' => 'showEditProduct'])->middleware('restrictAccess');

Route::post('/admin/products/edited/{id}', ['uses'=> 'Admin\ProductController@edited', 'as' => 'editedProduct'])->middleware('restrictAccess');

Route::get('/admin/products/deleted/{id}', ['uses' => 'Admin\ProductController@deleted', 'as' => 'deletedProduct'])->middleware('restrictAccess');

Route::get('/admin/categories', ['uses' => 'Admin\CategoryController@index', 'as' => 'adminCategories'])->middleware('restrictAccess');

Route::get('/admin/categories/add', ['uses'=> 'Admin\CategoryController@showAdd', 'as' => 'showAddCategories'])->middleware('restrictAccess');

Route::post('/admin/categories/added', ['uses'=> 'Admin\CategoryController@added', 'as' => 'addedCategories'])->middleware('restrictAccess');

Route::get('/admin/categories/deleted/{id}', ['uses'=> 'Admin\CategoryController@deleted', 'as' => 'deletedCategories'])->middleware('restrictAccess');

Route::get('/admin/categories/edit/{id}', ['uses'=> 'Admin\CategoryController@showEdit', 'as' => 'showEditCategories'])->middleware('restrictAccess');

Route::post('/admin/categories/edited/{id}', ['uses'=> 'Admin\CategoryController@edited', 'as' => 'editedCategories'])->middleware('restrictAccess');

Route::get('/admin/reviews', ['uses' => 'Admin\ReviewController@index', 'as' => 'adminReviews'])->middleware('restrictAccess');

Route::get('/admin/reviews/deleted/{id}', ['uses' => 'Admin\ReviewController@deleted', 'as' => 'deletedReviews'])->middleware('restrictAccess');

Route::get('/admin/notes', ['uses' => 'Admin\NoteController@index', 'as' => 'adminNotes'])->middleware('restrictAccess');

Route::get('/admin/notes/add', ['uses' => 'Admin\NoteController@add', 'as' => 'showAddNotes'])->middleware('restrictAccess');

Route::post('/admin/notes/added', ['uses' => 'Admin\NoteController@added', 'as' => 'addedNotes'])->middleware('restrictAccess');

Route::get('/admin/notes/edit/{id}', ['uses' => 'Admin\NoteController@edit', 'as' => 'showEditNotes'])->middleware('restrictAccess');

Route::post('/admin/notes/edited/{id}', ['uses' => 'Admin\NoteController@edited', 'as' => 'editedNotes'])->middleware('restrictAccess');

Route::get('/admin/notes/deleted/{id}', ['uses' => 'Admin\NoteController@deleted', 'as' => 'deletedNotes'])->middleware('restrictAccess');

Route::get('/admin/topics/', ['uses' => 'Admin\TopicController@index', 'as' => 'adminTopics'])->middleware('restrictAccess');

Route::get('/admin/topics/add', ['uses' => 'Admin\TopicController@add', 'as' => 'showAddTopic'])->middleware('restrictAccess');

Route::post('/admin/topics/added', ['uses' => 'Admin\TopicController@added', 'as' => 'addedTopics'])->middleware('restrictAccess');

Route::get('/admin/topics/edit/{id}', ['uses' => 'Admin\TopicController@edit', 'as' => 'showEditTopics'])->middleware('restrictAccess');

Route::post('/admin/topics/edited/{id}', ['uses' => 'Admin\TopicController@edited', 'as' => 'editedTopics'])->middleware('restrictAccess');

Route::get('/admin/topics/deleted/{id}', ['uses' => 'Admin\TopicController@deleted', 'as' => 'deletedTopics'])->middleware('restrictAccess');

Route::get('/admin/comments', ['uses' => 'Admin\CommentController@index', 'as' => 'adminComments'])->middleware('restrictAccess');

Route::get('/admin/comments/deleted/{id}', ['uses' => 'Admin\CommentController@deleted', 'as' => 'deletedComments'])->middleware('restrictAccess');

Route::get('/admin/users', ['uses' => 'Admin\UserController@index', 'as' => 'adminUsers'])->middleware('restrictAccess');

Route::get('/admin/users/edit/{id}', ['uses' => 'Admin\UserController@edit', 'as' => 'showEditUsers'])->middleware('restrictAccess');

Route::post('/admin/users/edited/{id}', ['uses' => 'Admin\UserController@edited', 'as' => 'editedUsers'])->middleware('restrictAccess');

Route::get('/admin/users/deleted/{id}', ['uses' => 'Admin\UserController@deleted', 'as' => 'deletedUsers'])->middleware('restrictAccess');
