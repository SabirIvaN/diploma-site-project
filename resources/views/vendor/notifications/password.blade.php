@component('mail::message')

@lang('Здравствуйте!')

<p>
    <b>Ваш новый пароль:</b>
    <h5>{{ $password }}</h5>
</p>
<p>
    В целях безопасности рекомендуем Вам никому не показывать и не говорить Ваш пароль.
</p>

{{-- Salutation --}}
@if (! empty($salutation))
{{ $salutation }}
@else
@lang('С уважением, ')<br>
{{ config('app.name') }}
@endif

@endcomponent
