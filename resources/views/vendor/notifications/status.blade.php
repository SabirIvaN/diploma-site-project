@component('mail::message')

@lang('Здравствуйте!')

<p>
    Ваш заказ №{{ $order->id }} {{ $order->status }}.
    @if($order->status == 'ожидает доставки')
    День доставки {{ $order->delivery_date }}
    @endif
</p>

{{-- Salutation --}}
@if (! empty($salutation))
{{ $salutation }}
@else
@lang('С уважением, ')<br>
{{ config('app.name') }}
@endif

@endcomponent
