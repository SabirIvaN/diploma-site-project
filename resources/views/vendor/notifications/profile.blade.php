@component('mail::message')

@lang('Здравствуйте!')

<p>
    <b>Ваши новые данные:</b> <br>
    Имя: {{ $content['name'] }}, <br>
    Отчество: {{ $content['patronymic'] }}, <br>
    Фамилия: {{ $content['surname'] }}, <br>
    Пол: {{ $content['sex'] }}, <br>
    E-Mail: {{ $content['email'] }}, <br>
    Телефон: {{ $content['phone'] }}, <br>
    Адрес: {{ $content['address'] }}.
</p>

{{-- Salutation --}}
@if (! empty($salutation))
{{ $salutation }}
@else
@lang('С уважением, ')<br>
{{ config('app.name') }}
@endif

@endcomponent

