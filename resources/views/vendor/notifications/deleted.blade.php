@component('mail::message')

@lang('Здравствуйте!')

<p>
    Ваш аккаунт удален.
</p>

{{-- Salutation --}}
@if (! empty($salutation))
{{ $salutation }}
@else
@lang('С уважением, ')<br>
{{ config('app.name') }}
@endif

@endcomponent
