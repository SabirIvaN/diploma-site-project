@component('mail::message')

@lang('Здравствуйте!')

<h1>Заказ №{{ $order->id }}</h1>
<p>
    Вы сделали заказ на сумму ₽{{ $order->price }}. Он {{ $order->status }}.
</p>
<p>
    В заказ входят следующие товары.
</p>
<table style="border-collapse: collapse; border: 1px solid rgb(113, 128, 150); text-align: center;">
    <thead style="border: 1px solid rgb(113, 128, 150);">
        <tr style="border: 1px solid rgb(113, 128, 150);">
            <th style="border: 1px solid rgb(113, 128, 150); text-align: left;">Название</th>
            <th style="border: 1px solid rgb(113, 128, 150);">Цена за 1 шт.</th>
            <th style="border: 1px solid rgb(113, 128, 150);">Количиство</th>
            <th style="border: 1px solid rgb(113, 128, 150);">Цена за все</th>
        </tr>
    </thead>
    <tbody style="border: 1px solid rgb(113, 128, 150);">
        @foreach($items as $item)
        <tr style="border: 1px solid rgb(113, 128, 150);">
            <td style="border: 1px solid rgb(113, 128, 150); text-align: left">{{ $item->name }}</td>
            <td style="border: 1px solid rgb(113, 128, 150);">₽{{ $item->price }}</td>
            <td style="border: 1px solid rgb(113, 128, 150);">{{ $item->quantity }}</td>
            <td style="border: 1px solid rgb(113, 128, 150);">₽{{ $item->total_price }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
<p>Итого: ₽{{ $order->price }}</p>

{{-- Salutation --}}
@if (! empty($salutation))
{{ $salutation }}
@else
@lang('С уважением, ')<br>
{{ config('app.name') }}
@endif

@endcomponent
