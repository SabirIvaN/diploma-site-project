@extends('layouts.main')

@section('title', 'Верификация')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card card-user mt-4">
                <div class="card-header">{{ __('Подтвердите свой E-Mail') }}</div>
                <div class="card-body">
                    @if (session('resent'))
                    <div class="alert alert-success" role="alert">
                        {{ __('Ссылка отправлена на Ваш E-Mail') }}
                    </div>
                    @endif
                    {{ __('Если Вы не получили ссылку, попробуйте еще раз') }},
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit"
                            class="btn btn-link p-0 m-0 align-baseline">{{ __('Нажмите сюда чтобы отправить еще одну ссылку') }}</button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
