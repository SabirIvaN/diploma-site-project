@extends('layouts.main')

@section('title')
{{ $product->name }}
@endsection

@section('content')
<section class="product container mt-4 mb-5">
    <h2>{{ $product->name }}</h2>
    <div class="product-row row d-flex flex-wrap mt-4">
        <div class="product-col col-md-5">
            <img class="product-main-img rounded bd-placeholder-img card-img-top" width="100%" height="180"
                src="{{ Storage::disk('local')->url('img/' . $product->image) }}">
        </div>
        <div class="product-col col-md-5 d-flex flex-column justify-content-between flex-wrap">
            <p>{{ $product->description }}</p>
            <p>₽{{ $product->price }}</p>
            <form action="/products/addToCartFromProduct/{{ $product->id }}" method="GET" id="formProduct">
                <input class="product-input form-control w-28 mb-5" type="number" min="1" value="1" name="quantity"
                    id="quantity">
                <button class="product-btn btn btn-primary w-28" type="submit">Добавить в корзину</button>
            </form>
        </div>
    </div>
</section>
@if(Auth::user())
<section class="container justify-content-center w-50 mb-5">
    <div class="row justify-content-center">
        <div class="mt-5" id="review-form">
            <form action="/products/addReview/{{ $product->id }}" method="POST" id="formReview">
                {{ csrf_field() }}
                <div class="form-group row align-items-center">
                    <label for="evaluation">Оценка:</label>
                    <input class="evaluation-input form-control" type="number" min="1" max="5" name="evaluation"
                        id="evaluation">
                </div>
                <div class="form-group row">
                    <label for="content">Отзыв:</label>
                    <textarea class="form-control" name="content" id="content" cols="50" rows="10"></textarea>
                </div>
                <div class="form-group row">
                    <button class="btn btn-primary" type="submit">Добавить отзыв</button>
                </div>
            </form>
        </div>
    </div>
    </div>
</section>
@endif
<section class="container w-50">
    <div class="row justify-content-center">
        <span class="d-none" id="routeProduct">{{ route('product', ['id' => $product->id]) }}</span>
        <div class="w-100" id="reviews">
            @foreach($reviews as $review)
            <div class="card w-100 mb-3">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <span>
                        {{ $review->full_name }} <br>
                        <small>
                            {{ $review->email }}
                        </small>
                    </span>
                    <span class="h5">
                        {{ $review->evaluation }}
                    </span>
                    <span>
                        {{ $review->created_at }}
                    </span>
                </div>
                <div class="card-body">
                    <p>
                        {{ $review->content }}
                    </p>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
<script src="{{ asset('../js/ajax-product.js') }}"></script>
<script src="{{ asset('../js/ajax-reviews.js') }}"></script>
@endsection
