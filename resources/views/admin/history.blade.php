@extends('layouts.admin')

@section('title', 'Панель управления | История заказов')

@section('content')
<section class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-1 mb-4 border-bottom">
    <h2 class="mb-1 font-weight-bold">История заказов</h2>
</section>
<section class="font-size-adaptive">
    @foreach($orders as $order)
    <div class="card mb-3 w-100 rounded shadow-sm">
        <div class="card-body">
            <div class="row d-flex justify-content-between align-items-center flex-wrap text-center">
                <div class="col-sm-1 mt-1 mb-1">
                    <h5 class="h5-adaptive m-0 font-weight-bold">{{ $order->id }}</h5>
                </div>
                <div class="col-sm-3 mt-1 mb-1">
                    <p class="font-weight">
                        Заказчик: {{ $order->user->name }} {{ $order->user->patronymic }} {{ $order->user->surname }}
                    </p>
                    <p class="font-weight">
                        Получатель: {{ $order->name }} {{ $order->patronymic }} {{ $order->surname }}
                    </p>
                </div>
                <div class="col-sm-2 mt-1 mb-1">
                    <span>{{ $order->email }}</span>
                </div>
                <div class="col-sm-2 mt-1 mb-1">
                    <span>{{ $order->status }}</span>
                </div>
                <div class="col-sm-2 mt-1 mb-1 d-flex flex-column">
                    <a class="btn btn-primary btn-font-size-admin mt-1 mb-1 mx-auto"
                        href="/admin/orders/information/{{ $order->id }}">Информация</a>
                    <a class="btn btn-danger btn-width-admin btn-font-size-admin mt-1 mb-1 mx-auto"
                        href="/admin/orders/delete/{{ $order->id }}">Удалить</a>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</section>
@endsection
