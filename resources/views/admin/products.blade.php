@extends('layouts.admin')

@section('title', 'Панель администратора | Товары')

@section('content')
<section class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-1 mb-4 border-bottom">
    <h2 class="mb-1 font-weight-bold">Товары</h2>
    <div class="btn-toolbar">
        <a class="btn btn-primary btn-font-size-admin" href="/admin/products/add">Добавить</a>
    </div>
</section>
<section class="container cards">
    @foreach($products as $product)
    <div class="card mb-3 w-100 shadow-sm">
        <div class="card-body">
            <div class="row d-flex justify-content-between align-items-center flex-wrap text-center pl-2 pr-2">
                <div class="col-sm-1 mt-1 mb-1">
                    <img class="product-cart-img rounded bd-placeholder-img card-img-top" width="100%" height="180"
                        src="{{ Storage::disk('local')->url('img/' . $product->image) }}">
                </div>
                <div class="col-sm-2 mt-1 mb-1">
                    <span>{{ $product->name }}</span>
                </div>
                <div class="col-sm-2 mt-1 mb-1">
                    <span>₽{{ $product->price }}</span>
                </div>
                <div class="col-sm-2 mt-1 mb-1 d-flex flex-column">
                    <a class="btn btn-primary btn-width-admin btn-font-size-admin mt-1 mb-1 mx-auto"
                        href="/admin/products/edit/{{ $product->id }}">Изменить</a>
                    <a class="btn btn-danger btn-width-admin btn-font-size-admin mt-1 mb-1 mx-auto"
                        href="/admin/products/deleted/{{ $product->id }}">Удалить</a>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</section>
@endsection
