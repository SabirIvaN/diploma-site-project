@extends('layouts.admin')

@section('title')
Панель управления | Информация о заказе №{{ $order->id }}
@endsection

@section('content')
<section class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-1 mb-4 border-bottom">
    <h2 class="mb-1 font-weight-bold">Информация о закезе №{{ $order->id }}</h2>
</section>
<section class="font-size-adaptive">
    <div class="form-group row">
        <span class="col-md-2 font-weight-bold">Заказчик:</span>
        <div class="col-md-5">
            <span>{{ $order->user->name }} {{ $order->user->patronymic }} {{ $order->user->surname }}</span>
        </div>
    </div>
    <div class="form-group row">
        <span class="col-md-2 font-weight-bold">Получатель:</span>
        <div class="col-md-5">
            <span>{{ $order->name }} {{ $order->patronymic }} {{ $order->surname }}</span>
        </div>
    </div>
    <div class="form-group row">
        <span class="col-md-2 font-weight-bold">Состав:</span>
        <div class="col-md-5">
            <table class="table table-sm table-borderless">
                <thead>
                    <tr>
                        <th scope="col">Название</th>
                        <th scope="col">Цена за 1 шт.</th>
                        <th scope="col">Количиство</th>
                        <th scope="col">Цена за все</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($order->items as $item)
                    <tr>
                        <td>{{ $item->name }}</td>
                        <td>₽{{ $item->price }}</td>
                        <td>{{ $item->quantity }}</td>
                        <td>₽{{ $item->total_price }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="form-group row">
        <span class="col-md-2 font-weight-bold">Итоговая цена:</span>
        <div class="col-md-5">
            <span>₽{{ $order->price }}</span>
        </div>
    </div>
    <div class="form-group row">
        <span class="col-md-2 font-weight-bold">Дата доставки:</span>
        <div class="col-md-5">
            <span>
                @if($order->status == null)
                не определена
                @else
                {{ $order->delivery_date }}
                @endif
            </span>
        </div>
    </div>
    <div class="form-group row">
        <span class="col-md-2 font-weight-bold">Адрес получателя:</span>
        <div class="col-md-5">
            <span>{{ $order->address }}</span>
        </div>
    </div>
    <div class="form-group row">
        <span class="col-md-2 font-weight-bold">Телефон получателя:</span>
        <div class="col-md-5">
            <span>{{ $order->phone }}</span>
        </div>
    </div>
    <div class="form-group row">
        <span class="col-md-2 font-weight-bold">E-Mail получателя:</span>
        <div class="col-md-5">
            <span>{{ $order->email }}</span>
        </div>
    </div>
    <div class="form-group row">
        <span class="col-md-2 font-weight-bold">Дата создания:</span>
        <div class="col-md-5">
            <span>{{ $order->creation_date }}</span>
        </div>
    </div>
</section>
@endsection
