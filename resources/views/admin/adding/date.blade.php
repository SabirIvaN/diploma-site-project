@extends('layouts.admin')

@section('title', 'Панель управления | Добавить дату доставки')

@section('content')
<section class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-1 mb-4 border-bottom">
    <h2 class="mb-1 font-weight-bold">Выбрать дату доставки</h2>
</section>
<section class="font-size-adaptive">
    <form action="/admin/orders/date/added/{{ $order->id }}" method="POST">
        {{ csrf_field() }}
        <div class="form-group mb-3">
            <label for="delivery-date">Дата доставки</label>
            <input class="form-control" type="date" name="delivery-date" id="delivery-date">
        </div>
        <button class="btn btn-primary" type="submit">Сохранить</button>
    </form>
</section>
@endsection
