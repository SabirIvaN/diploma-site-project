@extends('layouts.admin')

@section('title', 'Панель управления | Добавить тему')

@section('content')
<section class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-1 mb-4 border-bottom">
    <h2 class="mb-1 font-weight-bold">Добавить тему</h2>
</section>
<section class="font-size-adaptive">
    <form action="/admin/topics/added" method="POST">
        {{ csrf_field() }}
        <div class="form-group mb-3">
            <label for="title">Название</label>
            <input class="form-control" type="text" name="name" id="name">
        </div>
        <button class="btn btn-primary" type="submit">Сохранить</button>
    </form>
</section>
@endsection
