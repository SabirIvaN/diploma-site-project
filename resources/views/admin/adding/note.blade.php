@extends('layouts.admin')

@section('title', 'Панель управления | Добавить запись')

@section('content')
<section class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-1 mb-4 border-bottom">
    <h2 class="mb-1 font-weight-bold">Добавить заметку</h2>
</section>
<section class="font-size-adaptive">
    <form action="/admin/notes/added" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-group mb-3">
            <label for="title">Название</label>
            <input class="form-control" type="text" name="title" id="title">
        </div>
        <div class="form-group mb-3">
            <label for="image">Изображение</label>
            <input class="form-control-file" type="file" name="image" id="image">
        </div>
        <div class="form-group mb-3">
            <label>
                <input type="checkbox" name="slider" id="slider">
                Добавить в слайдер
            </label>
        </div>
        <div class="form-group mb-3">
            <label for="category">Тема</label>
            <select class="form-control" name="topic" id="topic">
                @foreach($topics as $topic)
                <option>{{ $topic->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group mb-3">
            <label for="content">Содержание</label>
            <textarea class="form-control w-100 h-25" row="70" name="content" id="content"></textarea>
        </div>
        <button class="btn btn-primary" type="submit">Сохранить</button>
    </form>
</section>
@endsection
