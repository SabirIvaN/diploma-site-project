@extends('layouts.admin')

@section('title', 'Панель управления | Категории')

@section('content')
<section class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-1 mb-4 border-bottom">
    <h2 class="mb-1 font-weight-bold">Категории</h2>
    <div class="btn-toolbar">
        <a class="btn btn-primary btn-font-size-admin" href="/admin/categories/add">Добавить</a>
    </div>
</section>
<section class="font-size-adaptive">
    @foreach($categories as $category)
    <div class="card mb-3 w-100 rounded shadow-sm">
        <div class="card-body">
            <div class="row d-flex justify-content-between align-items-center flex-wrap text-left">
                <div class="col-sm-10 mt-1 mb-1">
                    <span>{{ $category->name }}</span>
                </div>
                <div class="col-sm-2 mt-1 mb-1 d-flex flex-column">
                    <a class="btn btn-primary btn-width-admin btn-font-size-admin mt-1 mb-1 mx-auto"
                        href="/admin/categories/edit/{{ $category->id }}">Изменить</a>
                    <a class="btn btn-danger btn-width-admin btn-font-size-admin mt-1 mb-1 mx-auto"
                        href="/admin/categories/deleted/{{ $category->id }}">Удалить</a>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</section>
@endsection
