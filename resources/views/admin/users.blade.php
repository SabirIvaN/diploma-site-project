@extends('layouts.admin')

@section('title', 'Панель администратора | Пользователи')

@section('content')
<section
    class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-1 mb-4 border-bottom">
    <h2 class="mb-1 font-weight-bold">Пользователи</h2>
</section>
<section class="table-responsive">
    <table class="table table-staff table-sm">
        <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">ФИО</th>
                <th scope="col">E-mail</th>
                <th scope="col">Права</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
            <tr>
                <th scope="row">{{ $user->id }}</th>
                <td>{{ $user->name . " " . $user->patronymic . " " . $user->surname }}</td>
                <td>{{ $user->email }}</td>
                <td>
                    @if($user->admin == 1)
                    Администратор
                    @else
                    Пользователь
                    @endif
                </td>
                <td>
                    @if($user->id == Auth::user()->id)
                    <button class="btn btn-secondary" disabled>Изменить</button>
                    @else
                    <a class="btn btn-primary" href="/admin/users/edit/{{ $user->id }}">Изменить</a>
                    @endif
                </td>
                <td>
                    @if($user->id == Auth::user()->id)
                    <button class="btn btn-secondary" disabled>Удалить</button>
                    @else
                    <a class="btn btn-danger" href="/admin/users/deleted/{{ $user->id }}">Удалить</a>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</section>
@endsection
