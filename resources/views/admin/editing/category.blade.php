@extends('layouts.admin')

@section('title')
Панель администратора | Изменить категорию "{{ $category->name }}"
@endsection

@section('content')
<section class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-1 mb-4 border-bottom">
    <h2 class="mb-1 font-weight-bold">Изменить категорию "{{ $category->name }}"</h2>
</section>
<section class="font-size-adaptive">
    <form action="/admin/categories/edited/{{ $category->id }}" method="POST">
        {{ csrf_field() }}
        <div class="form-group mb-3">
            <label for="title">Название</label>
            <input class="form-control" type="text" name="name" value="{{ $category->name }}" id="name">
        </div>
        <button class="btn btn-primary" type="submit">Сохранить</button>
    </form>
</section>
@endsection
