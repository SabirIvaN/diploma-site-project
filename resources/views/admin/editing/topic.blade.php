@extends('layouts.admin')

@section('title')
Панель управления | Изменить тему "{{ $topic->name }}"
@endsection

@section('content')
<section class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-1 mb-4 border-bottom">
    <h2 class="mb-1 font-weight-bold">Изменить тему "{{ $topic->name }}"</h2>
</section>
<section class="font-size-adaptive">
    <form action="/admin/topics/edited/{{ $topic->id }}" method="POST">
        {{ csrf_field() }}
        <div class="form-group mb-3">
            <label for="title">Название</label>
            <input class="form-control" type="text" value="{{ $topic->name }}" name="name" id="name">
        </div>
        <button class="btn btn-primary" type="submit">Сохранить</button>
    </form>
</section>
@endsection
