@extends('layouts.admin')

@section('title')
Панель управления | Изменить пользователя "{{ $user->name . ' ' . $user->patronymic . ' ' . $user->surname }}"
@endsection

@section('content')
<section class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-1 mb-4 border-bottom">
    <h2 class="mb-1 font-weight-bold">Изменить пользователя "{{ $user->name . ' ' . $user->patronymic . ' ' . $user->surname }}"</h2>
</section>
<section class="font-size-adaptive">
    <form action="/admin/users/edited/{{ $user->id }}" method="POST">
        {{ csrf_field() }}
        <div class="form-group mb-3">
            <label>
                <input type="checkbox" name="admin" id="admin">
                Администратор
            </label>
        </div>
        <button class="btn btn-primary" type="submit">Сохранить</button>
    </form>
</section>
@endsection
