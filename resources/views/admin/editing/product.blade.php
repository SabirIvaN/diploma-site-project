@extends('layouts.admin')

@section('title')
Панель администратора | Изменить товар "{{ $product->name }}"
@endsection

@section('content')
<section class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-1 mb-4 border-bottom">
    <h2 class="mb-1 font-weight-bold">Изменить товар "{{ $product->name }}"</h2>
</section>
<section class="font-size-adaptive">
    <form action="/admin/products/edited/{{ $product->id }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-group mb-3">
            <label for="title">Название</label>
            <input class="form-control" type="text" value="{{ $product->name }}" name="name" id="name">
        </div>
        <div class="form-group mb-3">
            <label for="price">Цена</label>
            <input class="form-control" type="text" value="{{ $product->price }}" name="price" id="price">
        </div>
        <div class="form-group mb-3">
            <label for="image">Изображение</label>
            <input class="form-control-file" type="file" name="image" id="image">
        </div>
        <div class="form-group mb-3">
            <label for="categories">Категория</label>
            <select class="form-control chosen-select" data-placeholder="Выберите категорию..." name="categories[]"
                id="categories" multiple>
                @foreach($categories as $category)
                <option>{{ $category->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group mb-3">
            <label for="description">Описание</label>
            <textarea class="form-control" rows="8" cols="80" name="description"
                id="description">{{ $product->description }}</textarea>
        </div>
        <button class="btn btn-primary" type="submit">Сохранить</button>
    </form>
</section>
@endsection
