@extends('layouts.admin')

@section('title')
Панель управления | Изменить дату доставки {{ $order->delivery_date }}
@endsection

@section('content')
<section class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-1 mb-4 border-bottom">
    <h2 class="mb-1 font-weight-bold">Изменить дату доставки {{ $order->delivery_date }}</h2>
</section>
<section class="font-size-adaptive">
    <form action="/admin/orders/date/edited/{{ $order->id }}" method="POST">
        {{ csrf_field() }}
        <div class="form-group mb-3">
            <label for="delivery-date">Дата доставки</label>
            <input class="form-control" type="date" value="{{ $order->delivery_date }}" name="delivery-date" id="delivery-date">
        </div>
        <button class="btn btn-primary" type="submit">Сохранить</button>
    </form>
</section>
@endsection
