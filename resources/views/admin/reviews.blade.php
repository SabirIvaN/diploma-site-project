@extends('layouts.admin')

@section('title', 'Панель управления | Отзывы')

@section('content')
<section class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-1 mb-4 border-bottom">
    <h2 class="mb-1 font-weight-bold">Отзывы</h2>
</section>
<section class="container cards">
    @foreach($reviews as $key => $review)
    <h3>{{ $key }}</h3>
    @for($i = 0; $i < count($review); $i++)
    <div class="card mb-3 w-100 shadow-sm">
        <div class="card-header">
            <span class="mr-5">{{ $review[$i]->full_name }}</span>
            <span>{{ $review[$i]->email }}</span>
        </div>
        <div class="card-body">
            <div class="row d-flex justify-content-between align-items-center flex-wrap text-center pl-2 pr-2">
            <div class="col-sm-1 mt-1 mb-1">
                    <span class="card-title">{{ $review[$i]->evaluation }}</span>
                </div>
                <div class="col-sm-9 mt-1 mb-1 text-left">
                    <p>{{ $review[$i]->content }}</p>
                </div>
                <div class="col-sm-2 mt-1 mb-1 d-flex flex-column">
                    <a class="btn btn-danger btn-width-admin btn-font-size-admin mt-1 mb-1 mx-auto"
                        href="/admin/reviews/deleted/{{ $review[$i]->id }}">Удалить</a>
                </div>
            </div>
        </div>
    </div>
    @endfor
    @endforeach
</section>
@endsection

