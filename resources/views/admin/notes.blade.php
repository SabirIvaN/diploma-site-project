@extends('layouts.admin')

@section('title', 'Панель управления | Записи в блоге')

@section('content')
<section class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-1 mb-4 border-bottom">
    <h2 class="mb-1 font-weight-bold">Заметки</h2>
    <div class="btn-toolbar">
        <a class="btn btn-primary btn-font-size-admin" href="/admin/notes/add">Добавить</a>
    </div>
</section>
<section class="font-size-adaptive">
    @foreach($notes as $note)
    <div class="card mb-3 w-100 rounded shadow-sm">
        <div class="card-body">
            <div class="row d-flex justify-content-between align-items-center flex-wrap text-center">
                <div class="col-sm-1 mt-1 mb-1">
                    <h5 class="h5-adaptive m-0 font-weight-bold">{{ $note->id }}</h5>
                </div>
                <div class="col-sm-2 mt-1 mb-1">
                    <span class="font-weight">{{ $note->title }}</span>
                </div>
                <div class="col-sm-2 mt-1 mb-1">
                    <span>{{ $note->author }}</span>
                </div>
                <div class="col-sm-2 mt-1 mb-1">
                    <span>{{ $note->created_at }}</span>
                </div>
                <div class="col-sm-2 mt-1 mb-1 d-flex flex-column">
                    <a class="btn btn-primary btn-width-admin btn-font-size-admin mt-1 mb-1 mx-auto"
                        href="/admin/notes/edit/{{ $note->id }}">Изменить</a>
                    <a class="btn btn-danger btn-width-admin btn-font-size-admin mt-1 mb-1 mx-auto"
                        href="/admin/notes/deleted/{{ $note->id }}">Удалить</a>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</section>
@endsection
