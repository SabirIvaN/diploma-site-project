@extends('layouts.main')

@section('title', 'Каталог')

@section('content')
<div class="container">
    <div class="row">
        <aside class="col-md-2">
            <ul class="categories nav">
                @foreach($categories as $category)
                <li class="nav-item category-item">
                    <a class="nav-link" href="/products/category/{{ $category->id }}">{{ $category->name }}</a>
                </li>
                @endforeach
            </ul>
        </aside>
        <div class="col-md-9">
            <section class="search-products mt-4">
                <form class="row" action="/products/search" method="GET">
                    <div class="form-group col-md-5">
                        <label for="price">Цена</label>
                        <div class="d-flex">
                            <input class="form-control" type="text" name="minPrice" id="minPrice">
                            <span class="ml-1 mr-1">–</span>
                            <input class="form-control" type="text" name="maxPrice" id="maxPrice">
                        </div>
                    </div>
                    <div class="form-group col-md-7">
                        <label for="search">Поиск</label>
                        <div class="d-flex">
                            <input class="form-control mr-3" type="search" name="search" id="search">
                            <button class="btn btn-success mb-3" type="submit">Найти</button>
                        </div>
                    </div>
                </form>
            </section>
            <section class="products">
                <div class="cards" id="products">
                    @foreach($products as $product)
                    <div class="card-product card mb-4">
                        <img class="product-img bd-placeholder-img card-img-top" width="100%" height="180"
                            src="{{ Storage::disk('local')->url('img/' . $product->image) }}">
                        <div class="card-body d-flex flex-column">
                            <a href="/products/{{ $product->id }}" class="h5 card-title">{{ $product->name }}</a>
                            <div class="d-flex justify-content-between">
                                <a class="btn btn-primary ajaxGet" href="#">
                                    {{ csrf_field() }}
                                    Добавить в корзину
                                    <span class="d-none"
                                        id="url">{{ route('addToCartFromCatalog', ['id' => $product->id]) }}</span>
                                    <span class="d-none" id="productId">{{ $product->id }}</span>
                                </a>
                                <span>₽{{ $product->price }}</span>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </section>
        </div>
    </div>
</div>
<script src="{{ asset('../js/ajax-products.js') }}"></script>
@endsection
