@extends('layouts.main')

@section('title', 'Новости')

@section('content')
<section class="jumbotron bg-white py-sm-5 mb-0">
    <div class="container d-flex flex-column">
        <h2 class="text-header-1 text-center mx-auto mb-3 w-15">Блог</h2>
        @if(!$topics->isEmpty())
        <a class="text-center mx-auto mb-3 w-15" data-toggle="collapse" href="#categories" aria-expanded="false"
            aria-controls="categories">Категории ↓</a>
        <div class="collapse row flex-inline mx-auto justify-content-center w-75 mb-3 pl-5" id="categories">
            @foreach($topics as $topic)
            <a class="mr-5" href="/notes/topic/{{ $topic->id }}">{{ $topic->name }}</a>
            @endforeach
        </div>
        @endif
    </div>
</section>
<section class="container">
    <div class="cards">
        @foreach($notes as $note)
        <article class="note card mx-auto mb-5 rounded border shadow-sm">
            <img class="card-img card-img-top img-fluid bd-placeholder-img mb-3" width="140" height="140"
                src="{{ Storage::disk('local')->url('img/' . $note->image) }}">
            <div class="card-body">
                <h5 class="card-title">{{ $note->title}}</h5>
                <div class="card-text mb-3">
                    <span>{!! rtrim(substr(strip_tags($note->text), 0, 200), '!,.-') . '...' !!}</span>
                </div>
                <div class="row row-cols-2">
                    <div class="col-md-6">
                        <a class="btn btn-primary" href="/notes/{{ $note->id }}">ЧИТАТЬ</a>
                    </div>
                    <div class="col">
                        <p class="card-text text-right">
                            <small class="text-muted align-middle w-100">{{ $note->created_at }}</small>
                        </p>
                    </div>
                </div>
            </div>
        </article>
        @endforeach
    </div>
</section>
@endsection
