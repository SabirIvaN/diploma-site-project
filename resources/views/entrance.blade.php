@extends('layouts.main')

@section('title', 'Главная страница')

@section('content')
<section class="carousel slide mb-5" id="carouselExampleIndicators" data-ride="carousel">
    <ol class="carousel-indicators">
        @foreach($slides as $key => $slide)
        <li class="{{ $key == 0 ? 'active' : '' }}" data-target="#carouselExampleIndicators" data-slide-to="{{ $key }}">
        </li>
        @endforeach
    </ol>
    <div class="carousel-inner">
        @foreach($slides as $key => $slide)
        <div class="{{ $key == 0 ? 'active' : '' }} carousel-item">
            <img class="slider-image bd-placeholder-img bd-placeholder-img-lg d-block w-100" width="800" height="400"
                src="{{ Storage::disk('local')->url('img/' . $slide->image) }}">
            <div class="carousel-caption d-none d-md-block">
                <a class="carousel-title h5" href="/slide/{{ $slide->id }}">{{ $slide->title }}</a>
                <p>{!! rtrim(substr(strip_tags($slide->text), 0, 100), '!,.-') . '...' !!}</p>
            </div>`
        </div>
        @endforeach
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Предыдущий слайд</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Следующий слайд</span>
    </a>
</section>
<section class="marketing container mb-5">
    <div class="row">
        <div class="col-lg-4 text-center">
            <svg class="bd-placeholder-img rounded-circle mb-3" width="140" height="140"
                xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img"
                aria-label="Placeholder: 140x140">
                <title>Placeholder</title>
                <rect width="100%" height="100%" fill="#777"></rect><text x="50%" y="50%" fill="#777"
                    dy=".3em">140x140</text>
            </svg>
            <h2>Заголовок</h2>
            <p>Далеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты.</p>
        </div>
        <div class="col-lg-4 text-center">
            <svg class="bd-placeholder-img rounded-circle mb-3" width="140" height="140"
                xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img"
                aria-label="Placeholder: 140x140">
                <title>Placeholder</title>
                <rect width="100%" height="100%" fill="#777"></rect><text x="50%" y="50%" fill="#777"
                    dy=".3em">140x140</text>
            </svg>
            <h2>Заголовок</h2>
            <p>Далеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты.</p>
        </div>
        <div class="col-lg-4 text-center">
            <svg class="bd-placeholder-img rounded-circle mb-3" width="140" height="140"
                xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img"
                aria-label="Placeholder: 140x140">
                <title>Placeholder</title>
                <rect width="100%" height="100%" fill="#777"></rect><text x="50%" y="50%" fill="#777"
                    dy=".3em">140x140</text>
            </svg>
            <h2>Заголовок</h2>
            <p>Далеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты.</p>
        </div>
    </div>
</section>
<section class="new-products container mb-2">
    <a class="h2 d-inline-block mb-3" href="/catalog">Новые товары ›</a>
    <div class="cards">
        @foreach($products as $product)
        <div class="card-new-product card mb-4">
            {{ csrf_field() }}
            <img class="product-new-img bd-placeholder-img card-img-top" width="100%" height="180"
                src="{{ Storage::disk('local')->url('img/' . $product->image) }}">
            <div class="card-body d-flex flex-column">
                <a class="card-title h5" href="/products/{{ $product->id }}">{{ $product->name }}</a>
                <div class="d-flex justify-content-between">
                    <a class="btn btn-primary ajaxGet" href="#">
                        Добавить в корзину
                        <span class="d-none" id="url">{{ route('addToCartFromCatalog', ['id' => $product->id]) }}</span>
                        <span class="d-none" id="productId">{{ $product->id }}</span>
                    </a>
                    <span>₽{{ $product->price }}</span>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</section>
<section class="news container mb-5 ">
    <a class="h2 d-inline-block mb-3" href="#">Последние новости ›</a>
    <div class="cards">
        @foreach($notes as $note)
        <div class="card-last-new card bg-dark mb-3 text-white">
            <img class="last-new-image card-last-new-img bd-placeholder-img card-img-top" width="100%" height="180"
                src="{{ Storage::disk('local')->url('img/' . $note->image) }}">
            <div class="card-img-overlay d-flex flex-column">
                <a class="last-new-title card-title h5" href="/note/{{ $note->id }}">{{ $note->title }}</a>
                <p class="card-text">{!! rtrim(substr(strip_tags($note->text), 0, 50), '!,.-') . '...' !!}</p>
                <div class="d-flex justify-content-between h-100 align-items-end">
                    <p class="card-created card-text m-0">{{ $note->created_at }}</p>
                    <p class="card-author card-text m-0">{{ $note->author }}</p>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</section>
<script src="{{ asset('../js/ajax-products.js') }}"></script>
@endsection
