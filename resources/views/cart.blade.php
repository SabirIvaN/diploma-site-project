@extends('layouts.main')

@section('title', 'Корзина')

@section('content')
<section
    class="container d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-1 mb-4 border-bottom">
    <h2 class="mb-1 font-weight-bold">Корзина</h2>
    <div class="btn-toolbar">
        <a class="btn btn-primary btn-font-size-admin" href="/cart/order">Оформить заказ</a>
    </div>
</section>
<section class="container cards">
    <div class="w-100" id="items">
        @foreach($cartItems->items as $item)
        <div class="card mb-3 w-100 shadow-sm">
            {{ csrf_field() }}
            <div class="card-body">
                <div class="row d-flex justify-content-between align-items-center flex-wrap text-center">
                    <div class="col-sm-2 mt-1 mb-1">
                        <img class="product-cart-img rounded bd-placeholder-img card-img-top" width="100%" height="180"
                            src="{{ Storage::disk('local')->url('img/' . $item['data']['image']) }}">
                    </div>
                    <div class="col-sm-2 mt-1 mb-1">
                        <span>{{ $item['data']['name'] }}</span>
                    </div>
                    <div class="col-sm-2 mt-1 mb-1">
                        <span>₽{{ $item['data']['price'] }}</span>
                    </div>
                    <span class="d-none" id="routeCart">{{ route('cart') }}</span>
                    <span class="d-none" id="itemId">{{ $item['data']['id'] }}</span>
                    <div class="col-sm-1 mt-1 mb-1 p-0">
                        <div class="block-increase-decrease mx-auto">
                            <a class="ajaxGetDecrease value-button value-button-decrease m-0 p-0" href="#">
                                -
                                <span
                                    class="routeDecreaseProduct d-none">{{ route('decreaseProduct', ['id' => $item['data']['id']]) }}</span>
                            </a>
                            <input class="value-input-number m-0 p-0" type="text" name="quantity" id="quantity"
                                value="{{ $item['quantity'] }}">
                            <a class="ajaxGetIncrease value-button values-button-increase m-0 p-0" href="#">
                                +
                                <span
                                    class="routeIncreaseProduct d-none">{{ route('increaseProduct', ['id' => $item['data']['id']]) }}</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-2 mt-1 mb-1">
                        <span>₽{{ $item['totalSinglePrice'] }}</span>
                    </div>
                    <div class="col-sm-3 mt-1 mb-1">
                        <a class="btn btn-danger ajaxGetDeleteItems" href="#">
                            Удалить
                            <span
                                class="d-none" id="routeDeleteItems">{{ route('deleteItems', ['id' => $item['data']['id']]) }}</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</section>
<section class="container">
    <div class="w-100" id="totals">
        <div class="card-body text-center">
            <div class="row d-flex justify-content-end align-items-center flex-wrap text-center">
                <div class="col-sm-3 mt-1 mb-1">
                    <span class="font-weight-bold">Количество:</span>
                </div>
                <div class="col-sm-2 mt-1 mb-1">
                    <span id="totalQuantity">{{ $cartItems->totalQuantity }}</span>
                </div>
                <div class="col-sm-3 mt-1 mb-1">
                    <span class="font-weight-bold">Итого:</span>
                </div>
                <div class="col-sm-2 mt-1 mb-1">
                    <span>₽{{ $cartItems->totalPrice }}</span>
                </div>
                <span class="d-none" id="itemsQuantity">{{ count($cartItems->items) }}</span>
            </div>
        </div>
    </div>
</section>
<script src="{{ asset('../js/ajax-cart.js') }}"></script>
@endsection
