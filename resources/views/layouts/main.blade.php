@include('layouts.header')

<main class="page-main">

    @yield('content')

    @show
</main>

@include('layouts.footer')
