        <footer class="page-footer mt-auto py-3">
            <section class="container border-top text-center text-muted pt-5 pb-5">
                <p class="mt-3 mb-2">© 2020 ИП Иванов А. Г.</p>
            </section>
        </footer>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
        </script>
        </body>

        </html>
