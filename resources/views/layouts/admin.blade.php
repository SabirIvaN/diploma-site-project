@include('layouts.header')

<div class="container-fluid">
    <aside class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar border-right">
            <div class="sidebar-sticky pl-2 pr-2">
                <ul class="nav flex-column border-bottom ml-1 mb-3 pb-3">
                    <li class="nav-item">
                        <a class="nav-link text-dark {{ request()->is('admin/orders') ? 'link-admin-active' : null }}"
                            href="/admin/orders">Заказы</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-dark {{ request()->is('admin/returns') ? 'link-admin-active' : null }}"
                            href="/admin/returns">Возврат заказов</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-dark {{ request()->is('admin/history') ? 'link-admin-active' : null }}"
                            href="/admin/history">История заказов</a>
                    </li>
                </ul>
                <ul class="nav flex-column border-bottom ml-1 mb-3 pb-3">
                    <li class="nav-item">
                        <a class="nav-link text-dark {{ request()->is('admin/products') ? 'link-admin-active' : null }}"
                            href="/admin/products">Товары</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-dark {{ request()->is('admin/categories') ? 'link-admin-active' : null }}"
                            href="/admin/categories">Категории</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-dark {{ request()->is('admin/reviews') ? 'link-admin-active' : null }}"
                            href="/admin/reviews">Отзывы</a>
                    </li>
                </ul>
                <ul class="nav flex-column border-bottom ml-1 pb-3 mb-3">
                    <li class="nav-item">
                        <a class="nav-link text-dark {{ request()->is('admin/notes') ? 'link-admin-active' : null }}"
                            href="/admin/notes">Записи</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-dark {{ request()->is('admin/topics') ? 'link-admin-active' : null }}"
                            href="/admin/topics">Темы</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-dark {{ request()->is('admin/comments') ? 'link-admin-active' : null }}"
                            href="/admin/comments">Комментарии</a>
                    </li>
                </ul>
                <ul class="nav flex-column ml-1">
                    <li class="nav-item">
                        <a class="nav-link text-dark {{ request()->is('admin/users') ? 'link-admin-active' : null }}"
                            href="/admin/users">Пользователи</a>
                    </li>
                </ul>
            </div>
        </nav>
    </aside>
    <main class="page-main main-admin col-md-10 ml-sm-auto col-lg-10 px-2">
        @yield('content')

        @show
    </main>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
    integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
<script src="{{ asset('../js/chosen.jquery.min.js') }}"></script>
<script src="{{ asset('../js/main.js') }}"></script>
</body>
