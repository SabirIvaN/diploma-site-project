<!DOCTYPE html>
<html class="h-100" lang="ru">

<head>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Интернет-магазин – @yield('title')</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css">
    <link rel="stylesheet" href="{{ asset('../css/chosen.min.css') }}">
    <link rel="stylesheet" href="{{ asset('../css/main.css') }}">
    @if(request()->is('admin/notes/add') || request()->is('admin/notes/edit/*'))
    <link rel="stylesheet" href="{{ asset('../css/summernote-supplement.css') }}">
    @endif
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
</head>

<body class="d-flex flex-column h-100">
    <header class="page-header {{ request()->is('admin/*') ? 'page-header-admin' : null }}">
        <nav class="navbar navbar-expand-lg navbar-light bg-light shadow-sm">
            <a class="navbar-brand" href="#">Интернет-магазин</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler"
                aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarToggler">
                <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                    <li class="nav-item {{ request()->is('/') ? 'active' : null }}">
                        <a class="nav-link" href="/">Главная страница</a>
                    </li>
                    <li
                        class="nav-item {{ request()->is('products') ? 'active' : null }} {{ request()->is('products/*') ? 'active' : null }}">
                        <a class="nav-link" href="/products">Каталог товаров</a>
                    </li>
                    <li class="nav-item {{ request()->is('notes') ? 'active' : null }}">
                        <a class="nav-link" href="/notes">Заметки</a>
                    </li>
                </ul>
                <ul class="navbar-nav my-2 my-lg-0">
                    @guest
                    @if(Route::has('register'))
                    <li class="nav-item {{ request()->is('register') ? 'active' : null }}">
                        <a class="nav-link" href="/register">Регистрация</a>
                    </li>
                    @endif
                    <li class="nav-item {{ request()->is('login') ? 'active' : null }}">
                        <a class="nav-link" href="/login">Войти</a>
                    </li>
                    @else
                    <li class="nav-item">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle dropdown-toggle-outline" href="#"
                            role="button" data-toggle="dropdown">
                            {{ Auth::user()->name }}
                        </a>
                        <div class="dropdown-menu dropdown-menu-right mr-2" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('home') }}">Профиль</a>
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Выйти') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                    @endguest
                    <li class="nav-item {{ request()->is('cart') ? 'active' : null }}">
                        <a class="nav-link" href="/cart">Корзина</a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
