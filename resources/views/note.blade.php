@extends('layouts.main')

@section('title')
{{ $note->title }}
@endsection

@section('content')
<nav class="container" aria-label="breadcrumb">
    <ol class="breadcrumb mt-2 pt-3 mb-4">
        <li class="breadcrumb-item"><a href="/notes/">Наш блог</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{ $note->title }}</li>
    </ol>
</nav>
<section class="note mb-5">
    <h2 class="text-center mb-3">{{ $note->title }}</h2>
    <img class="card-img-note card-img-top img-fluid rounded mb-4 product-img bd-placeholder-img card-img-top"
        width="100%" height="180" src="{{ Storage::disk('local')->url('img/' . $note->image) }}">
    <p>{!! $note->text !!}</p>
    <div class="row row-cols-2">
        <div class="col">
            <span>{{ $note->author }}</span>
        </div>
        <div class="col d-flex justify-content-end align-items-center">
            <small class="text-muted">{{ $note->created_at }}</small>
        </div>
    </div>
</section>
@if(Auth::user())
<section class="container-sm justify-content-center w-75 mb-5">
    <div class="justify-content-center">
        <div class="mt-5" id="review-form">
            <form action="/notes/addComment/{{ $note->id }}" method="POST" id="formComments">
                {{ csrf_field() }}
                <div class="form-group row">
                    <label for="content">Комментарий:</label>
                    <textarea class="form-control" name="content" id="content" cols="50" rows="10"></textarea>
                </div>
                <div class="form-group row">
                    <button class="btn btn-primary" type="submit">Добавить комментарий</button>
                </div>
            </form>
        </div>
    </div>
    </div>
</section>
@endif
<section class="container-sm justify-content-center w-75">
    <span class="d-none" id="routeNote">{{ route('note', ['id' => $note->id]) }}</span>
    <div id="comments">
        <div class="row justify-content-center">
            @foreach($comments as $comment)
            <div class="card w-100 mb-3">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <span>
                        {{ $comment->full_name }} <br>
                        <small>
                            {{ $comment->email }}
                        </small>
                    </span>
                    <span>
                        {{ $comment->created_at }}
                    </span>
                </div>
                <div class="card-body">
                    <p>
                        {{ $comment->content }}
                    </p>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
<script src="{{ asset('../js/ajax-comments.js') }}"></script>
@endsection
