@extends('layouts.main')

@section('title', 'Панель пользователя')

@section('content')
<section
    class="container d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-1 mb-4 border-bottom">
    <h2 class="mb-1 font-weight-bold">{{ Auth::user()->name }} {{ Auth::user()->patronymic }}
        {{ Auth::user()->surname }}</h2>
    @if(Auth::user()->admin)
    <div class="btn-toolbar">
        <a class="btn btn-primary" href="/admin/orders">Панель администратора</a>
    </div>
    @endif
</section>
<section class="container">
    <div class="user-interface row">
        <div class="col-4">
            <div class="list-group" id="list-tab" role="tablist">
                <a class="list-group-item list-group-item-action active" id="list-profile-list" data-toggle="list"
                    href="#list-profile" role="tab" aria-controls="profile">Профиль</a>
                <a class="list-group-item list-group-item-action" id="list-orders-list" data-toggle="list"
                    href="#list-orders" role="tab" aria-controls="orders">Заказы</a>
            </div>
        </div>
        <div class="col-8">
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="list-profile" role="tabpanel"
                    aria-labelledby="list-profile-list">
                    <form action="#" method="POST">
                        <div class="form-group row d-flex">
                            <div class="col-sm-3 text-left pl-0">
                                <span class="col-sm-2 col-form-label text-left font-weight-bold">Имя</span>
                            </div>
                            <div class="col-sm-9">
                                <span class="col-sm-2 col-form-label text-left">{{ Auth::user()->name }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-3 text-left pl-0">
                                <span class="col-sm-2 col-form-label text-left font-weight-bold">Отчество</span>
                            </div>
                            <div class="col-sm-9">
                                <span class="col-sm-2 col-form-label text-left">{{ Auth::user()->patronymic }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-3 text-left pl-0">
                                <span class="col-sm-2 col-form-label text-left font-weight-bold">Фамилия</span>
                            </div>
                            <div class="col-sm-9">
                                <span class="col-sm-2 col-form-label text-left">{{ Auth::user()->surname }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-3 text-left pl-0">
                                <span class="col-sm-2 col-form-label text-left font-weight-bold">Пол</span>
                            </div>
                            <div class="col-sm-9">
                                <span class="col-sm-2 col-form-label text-left">{{ Auth::user()->sex }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-3 text-left pl-0">
                                <span class="col-sm-2 col-form-label text-left font-weight-bold">Телефон</span>
                            </div>
                            <div class="col-sm-9">
                                <span class="col-sm-2 col-form-label text-left">{{ Auth::user()->phone }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-3 text-left pl-0">
                                <span class="col-sm-2 col-form-label text-left font-weight-bold">Адрес</span>
                            </div>
                            <div class="col-sm-9">
                                <span class="col-sm-2 col-form-label text-left">{{ Auth::user()->address }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <a class="btn btn-primary ml-3 mr-3"
                                href="/home/user/profile/edit/{{ Auth::user()->id }}">Редактировать профиль</a>
                            <a class="btn btn-primary ml-3"
                                href="/home/user/password/edit/{{ Auth::user()->id }}">Редактировать пароль</a>
                            <a class="btn btn-danger ml-3" href="/home/user/send/{{ Auth::user()->id }}">Удалить
                                профиль</a>
                        </div>
                    </form>
                </div>
                <div class="tab-pane fade" id="list-orders" role="tabpanel" aria-labelledby="list-orders-list">
                    <section class="container cards">
                        @foreach($orders as $order)
                        <div class="card mb-3 w-100 shadow-sm">
                            <div class="card-body">
                                <div
                                    class="row d-flex justify-content-between align-items-center flex-wrap text-center">
                                    <div class="col-sm-3 mt-1 mb-1">
                                        <span>№{{ $order->id }}</span>
                                    </div>
                                    <div class="col-sm-3 mt-1 mb-1">
                                        <span>{{ $order->created_at }}</span>
                                    </div>
                                    <div class="col-sm-3 mt-1 mb-1">
                                        <span>{{ $order->status }}</span>
                                    </div>
                                    <div class="col-sm-2 mt-1 mb-1">
                                        <button class="btn" data-toggle="collapse" data-target="#collapse{{ $order->id }}"
                                            aria-expanded="false" aria-controls="collapse{{ $order->id }}">↓</button>
                                    </div>
                                </div>
                                <div class="collapse w-100 text-left ml-4" id="collapse{{ $order->id }}">
                                    <div class="row d-flex justify-content-start align-items-start flex-wrap">
                                        <div class="col-sm-12">
                                            <table class="table table-borderless">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Название товара</th>
                                                        <th scope="col">Цена за 1 штуку</th>
                                                        <th scope="col">Количество</th>
                                                        <th scope="col">Цена за все</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($order->items as $item)
                                                    <tr>
                                                        <td>{{ $item->name }}</td>
                                                        <td>₽{{ $item->price }}</td>
                                                        <td>{{ $item->quantity }}</td>
                                                        <td>₽{{ $item->total_price }}</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-sm-10 row">
                                            <div class="col-sm-5">
                                                <table class="table table-borderless">
                                                    <thead>
                                                        <tr>
                                                            <th>Итого:</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div class="col-sm-5">
                                                <table class="table table-borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>₽{{ $order->price }}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-sm-10 row">
                                            <div class="col-sm-5">
                                                <table class="table table-borderless">
                                                    <thead>
                                                        <tr>
                                                            <th>Оформлен:</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div class="col-sm-5">
                                                <table class="table table-borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>{{ $order->name }}</td>
                                                            <td>{{ $order->surname }}</td>
                                                            <td>{{ $order->patronymic }}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-sm-10 row">
                                            <div class="col-sm-5">
                                                <table class="table table-borderless">
                                                    <thead>
                                                        <tr>
                                                            <th>Адрес для доставки:</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div class="col-sm-5">
                                                <table class="table table-borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>{{ $order->address }}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-sm-10 row">
                                            <div class="col-sm-5">
                                                <table class="table table-borderless">
                                                    <thead>
                                                        <tr>
                                                            <th>Телефон для доставки:</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div class="col-sm-5">
                                                <table class="table table-borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>{{ $order->phone }}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-sm-10 row">
                                            <div class="col-sm-5">
                                                <table class="table table-borderless">
                                                    <thead>
                                                        <tr>
                                                            <th>Дата доставки:</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div class="col-sm-5">
                                                <table class="table table-borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                            @if($order->delivery_date == null)
                                                            не определена
                                                            @else
                                                            {{ $order->delivery_date }}
                                                            @endif
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-sm-11 d-flex justify-content-end">
                                            @if($order->status == 'ожидает оформления')
                                            <a class="btn btn-primary mr-1" href="/cart/order/registration/{{ $order->id }}">Оформить заказ</a>
                                            @endif
                                            @if($order->status == 'ожидает оплаты')
                                            <a class="btn btn-primary mr-1" href="/cart/order/checkout/{{ $order->id }}">Оплатить заказ</a>
                                            @endif
                                            @if(($order->status == 'ожидает оформления') || ($order->status == 'ожидает оплаты') || ($order->status == 'не оплачен') || ($order->status == 'отменен') || ($order->status == 'возвращен'))
                                            <a class="btn btn-danger mr-1" href="/cart/order/delete/{{ $order->id }}">Удалить заказ</a>
                                            @endif
                                            @if(($order->status == 'оплачен') || ($order->status == 'собирается'))
                                            <a class="btn btn-danger mr-1" href="/cart/order/refund/{{ $order->id }}">Отменить заказ</a>
                                            @endif
                                            @if($order->status == 'завершен')
                                            <a class="btn btn-danger" href="/cart/order/return/{{ $order->id }}">Вернуть заказ</a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </section>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
