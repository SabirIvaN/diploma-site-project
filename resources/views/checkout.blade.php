@extends('layouts.main')

@section('title', 'Оформить заказ')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card card-user mt-4">
                <div class="card-header">
                    <span>Оформить заказ</span>
                </div>
                <div class="card-body">
                    <form action="/cart/ordered/registration/{{ $order->id }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right" for="email">E-Mail</label>
                            <div class="col-md-6">
                                <input class="form-control" type="email" value="{{ Auth::user()->email }}" name="email" id="email">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right" for="name">Имя</label>
                            <div class="col-md-6">
                                <input class="form-control" type="text" value="{{ Auth::user()->name }}" name="name" id="name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right" for="patronymic">Отчество</label>
                            <div class="col-md-6">
                                <input class="form-control" type="text" value="{{ Auth::user()->patronymic }}" name="patronymic" id="patronymic">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right" for="surname">Фамилия</label>
                            <div class="col-md-6">
                                <input class="form-control" type="text" value="{{ Auth::user()->surname }}" name="surname" id="surname">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right" for="address">Адрес</label>
                            <div class="col-md-6">
                                <input class="form-control" type="text" value="{{ Auth::user()->address }}" name="address" id="address">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right" for="phone">Телефон</label>
                            <div class="col-md-6">
                                <input class="form-control" type="text" value="{{ Auth::user()->phone }}" name="phone" id="phone">
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button class="btn btn-primary" type="submit">Оплатить заказ</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
