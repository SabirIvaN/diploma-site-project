@extends('layouts.main')

@section('title')
Редактировать пароль пользователя
"{{ $user->name . " " . $user->patronymic . " " . $user->surname }}"
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card card-user mt-4">
                <div class="card-header">
                    <span>
                        Редиктирование пароль пользователя
                        "{{ $user->name . " " . $user->patronymic . " " . $user->surname }}"
                    </span>
                </div>
                <div class="card-body">
                    <form action="/home/user/password/edit/update/{{ $user->id }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right" for="old-password">
                                Старый пароль
                            </label>
                            <div class="md-6">
                                <input class="form-control" type="password" name="old-password" id="old-password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right" for="password">
                                Пароль
                            </label>
                            <div class="md-6">
                                <input class="form-control" type="password" name="password" id="password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right" for="password-confirm">
                                Подтверждение пароля
                            </label>
                            <div class="md-6">
                                <input class="form-control" type="password" name="password_confirmation"
                                    id="password-confirm">
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button class="btn btn-primary" type="submit">
                                    Сохранить
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
