@extends('layouts.main')

@section('title')
Редактировать профиль пользователя "{{ $user->name . " " . $user->patronymic . " " . $user->surname }}"
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card card-user mt-4">
                <div class="card-header">
                    <span>
                        Редиктирование профиля пользователя
                        "{{ $user->name . " " . $user->patronymic . " " . $user->surname }}"
                    </span>
                </div>
                <div class="card-body">
                    <form action="/home/user/profile/edit/save/{{ $user->id }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right" for="name">
                                Имя
                            </label>
                            <div class="md-6">
                                <input class="form-control" type="text" value="{{ $user->name }}" name="name" id="name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right" for="patronymic">
                                Отчество
                            </label>
                            <div class="md-6">
                                <input class="form-control" type="text" value="{{ $user->patronymic }}"
                                    name="patronymic" id="patronymic">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right" for="surname">
                                Фамилия
                            </label>
                            <div class="md-6">
                                <input class="form-control" type="text" value="{{ $user->surname }}" name="surname"
                                    id="surname">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right" for="sex">
                                Пол
                            </label>
                            <div class="md-6">
                                <select class="form-control" name="sex" id="sex">
                                    <option>мужской</option>
                                    <option>женский</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right" for="email">
                                E-Mail
                            </label>
                            <div class="md-6">
                                <input class="form-control" type="email" value="{{ $user->email }}" name="email"
                                    id="email">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right" for="phone">
                                Телефон
                            </label>
                            <div class="md-6">
                                <input class="form-control" type="phone" value="{{ $user->phone }}" name="phone"
                                    id="phone">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right" for="address">
                                Адрес
                            </label>
                            <div class="md-6">
                                <input class="form-control" type="text" value="{{ $user->address }}" name="address"
                                    id="address">
                            </div>
                        </div>
                        <div class="form-group mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button class="btn btn-primary" type="submit">
                                    Сохранить
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
