@extends('layouts.main')

@section('title')
Оплатить заказ №{{ $order->id }}
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card card-user mt-4">
                <div class="card-header">
                    <span>Выставление счета</span>
                </div>
                <div class="card-body">
                    <form action="#" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <table class="table table-borderless text-center">
                                <thead>
                                    <tr>
                                        <th class="text-left" scope="col">Название</th>
                                        <th scope="col">Цена за 1 шт.</th>
                                        <th scope="col">Количество</th>
                                        <th scope="col">Цена за все</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($items as $item)
                                    <tr>
                                        <td class="text-left">{{ $item->name }}</td>
                                        <td>₽{{ $item->price }}</td>
                                        <td>{{ $item->quantity }}</td>
                                        <td>₽{{ $item->total_price }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="form-group row">
                            <span class="col-md-5 text-md-right h5">Количество всех товаров:</span>
                            <span class="col-md-6 align-bottom">{{ $order->total_quantity }}</span>
                        </div>
                        <div class="form-group row">
                            <span class="col-md-5 text-md-right h5">Итоговая цена:</span>
                            <span class="col-md-6 align-bottom">₽{{ $order->price }}</span>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <a class="btn btn-primary" href="/cart/order/checkout/{{ $order->id }}">Выставить счет</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
