@extends('layouts.main')

@section('title', 'Оплата счета')

@section('content')
<script src="https://kassa.yandex.ru/checkout-ui/v2.js"></script>
<div class="mt-4" id="payment-form"></div>
<script>
const checkout = new window.YandexCheckout({
    confirmation_token: "{{ $payment->confirmation->confirmation_token }}",
    return_url: "{{ route('response', ['id' => $order->id]) }}",
    error_callback(error) {}
});
checkout.render('payment-form');
</script>
@endsection
