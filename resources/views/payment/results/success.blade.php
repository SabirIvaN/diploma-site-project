@extends('layouts.main')

@section('title', 'Заказ оплачен')

@section('content')
<div class="container text-center">
    <img class="mb-3" src="{{ asset('../images/success.svg') }}" width="400" height="400" alt="Заказ оплачен">
    <h3 class="mb-5">Заказ оплачен!</h3>
    <a class="btn btn-primary" href="/home">Вернуться в профиль</a>
</div>
@endsection
