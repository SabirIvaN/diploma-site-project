@extends('layouts.main')

@section('title', 'Заказ не оплачен')

@section('content')
<div class="container text-center">
    <img class="mb-3" src="{{ asset('../images/cancel.svg') }}" width="400" height="400" alt="Заказ не оплачен">
    <h3 class="mb-5">Заказ не оплачен!</h3>
    <a class="btn btn-primary" href="/home">Вернуться в профиль</a>
</div>
@endsection
