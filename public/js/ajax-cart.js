var routeCart = $("#routeCart").text();
    var itemId = $("#itemId").text();
    $(document).ready(function () {
        $(this).on("click", ".ajaxGetDecrease", function (e) {
            e.preventDefault();
            var url = $(this).find(".routeDecreaseProduct").text();
            $.ajax({
                method: "GET",
                url: url,
                data: {
                    id: itemId,
                },
                success: function (html) {
                    $("#items").load(routeCart + " #items");
                    $("#totals").load(routeCart + " #totals");
                },
            });
        });
    });
    $(document).ready(function () {
        $(this).on("click", ".ajaxGetIncrease", function (e) {
            e.preventDefault();
            var url = $(this).find(".routeIncreaseProduct").text();
            $.ajax({
                method: "GET",
                url: url,
                data: {
                    id: itemId,
                },
                success: function (html) {
                    $("#items").load(routeCart + " #items");
                    $("#totals").load(routeCart + " #totals");
                },
            });
        });
    });
    $(document).ready(function () {
        $(this).on("click", ".ajaxGetDeleteItems", function (e) {
            e.preventDefault();
            var url = $(this).find("#routeDeleteItems").text();
            $.ajax({
                method: "GET",
                url: url,
                data: {
                    id: itemId,
                },
                success: function (html) {
                    $("#items").load(routeCart + " #items");
                    $("#totals").load(routeCart + " #totals");
                    var itemsQuantity = $("#itemsQuantity").text();
                    if (itemsQuantity == 1) {
                        location.reload();
                    }
                },
            });
        });
    });
