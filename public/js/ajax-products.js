$(document).ready(function() {
    $(".ajaxGet").click(function(e) {
        e.preventDefault();
        var url = $(this).find("#url").text();
        var productId = $(this).find("#productId").text();
        var token = $(this).find("input[name='_token']").val();
        $.ajax({
            method: "GET",
            url: url,
            data: {
                _token: token,
                id: productId,
            },
        });
    });
});
