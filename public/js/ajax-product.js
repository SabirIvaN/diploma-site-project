$(document).ready(function () {
    $("#formProduct").on("submit", function (e) {
        e.preventDefault();
        var url = $("#formProduct").attr("action");
        $.ajax({
            method: "GET",
            url: url,
            data: $("#formProduct").serialize(),
        });
    });
});
