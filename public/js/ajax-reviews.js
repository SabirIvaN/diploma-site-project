$(document).ready(function () {
    $("#formReview").on("submit", function (e) {
        e.preventDefault();
        var url = $("#formReview").attr("action");
        var data = $("#formReview").serialize();
        var routeProduct = $("routeProduct").text();
        $.ajax({
            method: "POST",
            url: url,
            data: data,
            success: function(html) {
                $("#reviews").load(routeProduct + " #reviews");
                $("#evaluation").val("");
                $("#content").val("");
            },
        });
    });
});
