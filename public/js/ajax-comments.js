$(document).ready(function () {
    $("#formComments").on("submit", function (e) {
        e.preventDefault();
        var url = $("#formComments").attr("action");
        var data = $("#formComments").serialize();
        var routeNote = $("#routeNote").text();
        $.ajax({
            method: "POST",
            url: url,
            data: data,
            success: function (html) {
                $("#comments").load(routeNote + " #comments");
                $("#content").val("");
            },
        });
    });
});
