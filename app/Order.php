<?php

namespace App;

use App\User;
use App\Item;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'yandex_id', 'name', 'surname', 'patronymic', 'address', 'phone', 'email', 'total_quantity', 'creation_date', 'status', 'delivery_date', 'price',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function items()
    {
        return $this->hasMany(Item::class);
    }
}
