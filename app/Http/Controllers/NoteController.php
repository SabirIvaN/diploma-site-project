<?php

namespace App\Http\Controllers;

use App\Note;
use App\Topic;
use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NoteController extends Controller
{
    public function index()
    {
        $notes = Note::orderBy('id', 'desc')->get();
        $topics = Topic::all();
        return view('notes', ['notes' => $notes, 'topics' => $topics]);
    }

    public function topic($id)
    {
        $topics = Topic::all();
        $topic = Topic::find($id);
        $notes = $topic->notes;
        return view('notes', ['notes' => $notes, 'topics' => $topics]);
    }

    public function note($id)
    {
        $note = Note::find($id);
        $comments = $note->comments()->orderBy('id', 'desc')->get();
        return view('note', ['note' => $note, 'comments' => $comments]);
    }

    public function addComment(Request $request, $id)
    {
        $this->validate($request, [
            'content' => 'required|min:5|max:5000',
        ]);
        $note = Note::find($id);
        $comment = new Comment();
        $comment->full_name = Auth::user()->name . ' ' . Auth::user()->patronymic . ' ' . Auth::user()->surname;
        $comment->email = Auth::user()->email;
        $comment->content = $request->get('content');
        if (!$note->comments()->save($comment)) {
            abort(404);
        }
    }
}
