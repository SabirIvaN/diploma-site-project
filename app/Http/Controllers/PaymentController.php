<?php

namespace App\Http\Controllers;

use App\User;
use App\Order;
use App\Mail\OrderPaymentMail;
use App\Mail\FailedOrderPaymentMail;
use App\Mail\CanceledOrderMail;
use App\Mail\NotCanceledOrderMail;
use App\Mail\ConsideredReturnMail;
use YandexCheckout\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class PaymentController extends Controller
{
    private $shopId = '717376';
    private $secretKey = 'test_ipiCKBqbtOh1g_-ELgXHnbA5k4M7vqjOujgc6WV8p9c';

    public function showPage($id)
    {
        $order = Order::find($id);
        $items = $order->items;
        return view('payment.page', ['order' => $order, 'items' => $items]);
    }

    public function showCheckout($id)
    {
        $order = Order::find($id);
        if ($order->status == 'ожидает оплаты') {
            $client = new Client();
            $client->setAuth($this->shopId, $this->secretKey);
            $user = User::find(Auth::user()->id);
            $payment = $client->createPayment([
                'amount' => [
                    'value' => $order->price,
                    'currency' => 'RUB',
                ],
                'confirmation' => [
                    'type' => 'embedded',
                ],
                'capture' => true,
                'description' => 'Заказ №' . $order->id,
            ], uniqid('', true));
            $order->yandex_id = $payment->id;
            if ($user->orders()->save($order)) {
                return view('payment.checkout', ['order' => $order, 'payment' => $payment]);
            } else {
                abort(404);
            }
        }
        abort(404);
    }

    public function responseCheckout($id)
    {
        $order = Order::find($id);
        $client = new Client();
        $client->setAuth($this->shopId, $this->secretKey);
        $payment = $client->getPaymentInfo($order->yandex_id);
        if ($order->status == 'ожидает оплаты') {
            $user = User::find(Auth::user()->id);
            if ($payment->status == 'succeeded') {
                $order->status = 'оплачен';
                if ($user->orders()->save($order)) {
                    Mail::to($user->email)->send(new OrderPaymentMail($order));
                    return redirect('/cart/order/response/result/succeeded');
                } else {
                    abort(404);
                }
            }
            if ($payment->status === 'canceled') {
                $order->status = 'не оплачен';
                if ($user->orders()->save($order)) {
                    Mail::to($user->email)->send(new FailedOrderPaymentMail($order));
                    return redirect('/cart/order/response/result/canceled');
                } else {
                    abort(404);
                }
            }
        }
    }

    public function deleteOrder($id)
    {
        $order = Order::find($id);
        if (($order->status == 'ожидает оформления') || ($order->status == 'ожидает оплаты') || ($order->status  == 'не оплачен') || ($order->status == 'отменен') || ($order->status == 'возвращен')) {
            $order->items()->delete();
            $order->delete();
            return redirect('/home');
        } else {
            abort(404);
        }
    }

    public function orderRefund($id)
    {
        $order = Order::find($id);
        if (($order->status == 'оплачен') || ($order->status == 'собирается')) {
            $client = new Client();
            $client->setAuth($this->shopId, $this->secretKey);
            $user = User::find(Auth::user()->id);
            $idempotenceKey = uniqid('', true);
            $response = $client->createRefund([
                'payment_id' => $order->yandex_id,
                'amount' => [
                    'value' => $order->price,
                    'currency' => 'RUB',
                ],
            ], $idempotenceKey);
            $refund = $client->getRefundInfo($response->id);
            if ($refund->status == 'succeeded') {
                $order->status = 'отменен';
                if ($user->orders()->save($order)) {
                    Mail::to($user->email)->send(new CanceledOrderMail($order));
                } else {
                    abort(404);
                }
            }
            if ($refund->status == 'canceled') {
                $order->status = 'не отменен';
                if ($user->orders()->save($order)) {
                    Mail::to($user->email)->send(new NotCanceledOrderMail($order));
                } else {
                    abort(404);
                }
            }
            return redirect('/home');
        }
        abort(404);
    }

    public function returnOrder($id)
    {
        $order = Order::find($id);
        $user = User::find(Auth::user()->id);
        if ($order->status == 'завершен') {
            $order->status = 'рассматривается для возврата';
            if ($user->orders()->save($order)) {
                Mail::to($user->email)->send(new ConsideredReturnMail($order));
                return redirect('/home');
            } else {
                abort(404);
            }
        } else {
            abort(404);
        }
    }

    public function responseCancel()
    {
        return view('payment.results.cancel');
    }

    public function responseSuccess()
    {
        return view('payment.results.success');
    }
}
