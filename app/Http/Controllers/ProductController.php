<?php

namespace App\Http\Controllers;

use App\User;
use App\Cart;
use App\Order;
use App\Review;
use App\Product;
use App\Category;
use App\Item;
use App\Mail\OrderMail;
use App\Mail\OrderRegistrationMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::all();
        $categories = Category::all();
        return view('products', ['products' => $products, 'categories' => $categories]);
    }

    public function category($id)
    {
        $products = Category::find($id)->products;
        $categories = Category::All();
        return view('products', ['products' => $products, 'categories' => $categories]);
    }

    public function search(Request $request)
    {
        $minPrice = $request->get('minPrice');
        $maxPrice = $request->get('maxPrice');
        $categories = Category::all();
        $search = $request->get('search');
        if (is_null($minPrice) && is_null($maxPrice)) {
            $products = Product::where('name', 'Like', $search . '%')->paginate(15);
        } elseif (is_null($minPrice)) {
            $products = Product::where('name', 'Like', $search . '%')
                ->where('price', '<=', $maxPrice)
                ->paginate(15);
        } elseif (is_null($maxPrice)) {
            $products = Product::where('name', 'Like', $search . '%')
                ->where('price', '>=', $minPrice)
                ->paginate(15);
        } else {
            $products = Product::where('name', 'Like', $search . '%')
                ->where('price', '>=', $minPrice)
                ->where('price', '<=', $maxPrice)
                ->paginate(15);
        }
        return view('products', ['products' => $products, 'categories' => $categories]);
    }

    public function product($id)
    {
        $product = Product::find($id);
        $reviews = $product->reviews()->orderBy('id', 'desc')->get();
        return view('product', ['product' => $product, 'reviews' => $reviews]);
    }

    public function addToCartFromCatalog(Request $request, $id)
    {
        $prevCart = $request->session()->get('cart');
        $cart = new Cart($prevCart);
        $product = Product::find($id);
        $cart->addSingleItem($id, $product);
        $request->session()->put('cart', $cart);
    }

    public function addToCartFromProduct(Request $request, $id)
    {
        $quantityItems = $request->get('quantity');
        $prevCart = $request->session()->get('cart');
        $cart = new Cart($prevCart);
        $product = Product::find($id);
        $cart->addMultipleItem($id, $product, $quantityItems);
        $request->session()->put('cart', $cart);
    }

    public function cart()
    {
        $cartItems = Session::get('cart');
        if ($cartItems) {
            if ($cartItems->totalQuantity > 0) {
                return view('cart', ['cartItems' => $cartItems]);
            }
            return redirect('/products');
        } else {
            return redirect('/products');
        }
    }

    public function increaseProduct(Request $request, $id)
    {
        $prevCart = $request->session()->get('cart');
        $cart = new Cart($prevCart);
        $product = Product::find($id);
        $cart->addSingleItem($id, $product);
        $request->session()->put('cart', $cart);
        return redirect('/cart');
    }

    public function decreaseProduct(Request $request, $id)
    {
        $prevCart = $request->session()->get('cart');
        $cart = new Cart($prevCart);
        if ($cart->items[$id]['quantity'] > 1) {
            $product = Product::find($id);
            $cart->items[$id]['quantity'] = $cart->items[$id]['quantity'] - 1;
            $cart->items[$id]['totalSinglePrice'] = $cart->items[$id]['quantity'] * $product['price'];
            $cart->updatePriceAndQuantity();
            $request->session()->put('cart', $cart);
        }
    }

    public function deleteItems(Request $request, $id)
    {
        $cart = $request->session()->get('cart');
        if (array_key_exists($id, $cart->items)) {
            unset($cart->items[$id]);
        }
        $prevCart = $request->session()->get('cart');
        $updatedCart = new Cart($prevCart);
        $updatedCart->updatePriceAndQuantity();
        $request->session()->put('cart', $updatedCart);
        return redirect('/cart');
    }

    public function addReview(Request $request, $id)
    {
        $this->validate($request, [
            'content' => 'required|min:5|max:5000',
        ]);
        $product = Product::find($id);
        $review = new Review();
        $review->full_name = Auth::user()->name . ' ' . Auth::user()->patronymic . ' ' . Auth::user()->surname;
        $review->email = Auth::user()->email;
        $review->evaluation = $request->get('evaluation');
        $review->content = $request->get('content');
        if ($product->reviews()->save($review)) {
            return redirect('/products/' . $id);
        } else {
            abort(404);
        }
    }

    public function order()
    {
        $user = User::find(Auth::user()->id);
        $cart = Session::get('cart');
        if ($cart) {
            $order = new Order();
            $order->creation_date = date('Y-m-d H:i:s');
            $order->status = 'ожидает оформления';
            $order->price = $cart->totalPrice;
            $order->total_quantity = $cart->totalQuantity;
            if (!$user->orders()->save($order)) {
                abort(404);
            }
            foreach ($cart->items as $cartItem) {
                $item = new Item();
                $item->name = $cartItem['data']['name'];
                $item->price = $cartItem['data']['price'];
                $item->quantity = $cartItem['quantity'];
                $item->total_price = $cartItem['totalSinglePrice'];
                if (!$order->items()->save($item)) {
                    abort(404);
                }
            }
            Mail::to($user->email)->send(new OrderMail($order, $order->items));
            Session::forget('cart');
            return redirect('/cart/order/registration/' . $order->id);
        } else {
            return redirect('/products');
        }
    }

    public function orderRegistration($id)
    {
        $order = Order::find($id);
        if ($order->status == 'ожидает оформления') {
            return view('checkout', ['order' => $order]);
        }
        abort(404);
    }

    public function ordered(Request $request, $id)
    {
        $order = Order::find($id);
        if ($order->status == 'ожидает оформления') {
            $userId = Auth::user()->id;
            $user = User::find($userId);
            $order->name = $request->get('name');
            $order->surname = $request->get('surname');
            $order->patronymic = $request->get('patronymic');
            $order->address = $request->get('address');
            $order->phone = $request->get('phone');
            $order->email = $request->get('email');
            $order->status = 'ожидает оплаты';
            if ($user->orders()->save($order)) {
                Mail::to($user->email)->send(new OrderRegistrationMail($order));
                return redirect('/cart/order/payment/' . $id);
            } else {
                abort(404);
            }
        }
        abort(404);
    }
}
