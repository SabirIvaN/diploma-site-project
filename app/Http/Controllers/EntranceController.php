<?php

namespace App\Http\Controllers;

use App\Note;
use App\Product;
use Illuminate\Http\Request;

class EntranceController extends Controller
{
    public function index()
    {
        $slides = Note::where('slider', '=', 'on')->orderBy('id', 'desc')->take(3)->get();
        $notes = Note::orderBy('id', 'desc')->take(4)->get();
        $products = Product::orderBy('id', 'desc')->take(4)->get();
        return view('entrance', ['products' => $products, 'slides' => $slides, 'notes' => $notes]);
    }

    public function showSliderNote($id)
    {
        $note = Note::find($id);
        $comments = $note->comments()->orderBy('id', 'desc')->get();
        return view('note', ['note' => $note, 'comments' => $comments]);
    }

    public function showLastNote($id)
    {
        $note = Note::find($id);
        $comments = $note->comments()->orderBy('id', 'desc')->get();
        return view('note', ['note' => $note, 'comments' => $comments]);
    }
}
