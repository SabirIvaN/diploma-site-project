<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        return view('admin.users', ['users' => $users]);
    }

    public function edit($id)
    {
        $user = User::find($id);
        return view('admin.editing.user', ['user' => $user]);
    }

    public function edited(Request $request, $id)
    {
        $user = User::find($id);
        $admin = (boolean) $request->get('admin');
        $user->admin = $admin;
        if ($user->save()) {
            return redirect('/admin/users');
        } else {
            abort(404);
        }
    }

    public function deleted($id)
    {
        User::destroy($id);
        return redirect('/admin/users/');
    }
}
