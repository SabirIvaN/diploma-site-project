<?php

namespace App\Http\Controllers\Admin;

use App\Topic;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\NoteController;
use Illuminate\Http\Request;

class TopicController extends Controller
{
    public function index()
    {
        $topics = Topic::all();
        return view('admin.topics', ['topics' => $topics]);
    }

    public function add(Request $request)
    {
        return view('admin.adding.topic');
    }

    public function added(Request $request)
    {
        $topic = new Topic();
        $topic->name = $request->get('name');
        $topic->save();
        return redirect('/admin/topics');
    }

    public function edit($id)
    {
        $topic = Topic::find($id);
        return view('admin.editing.topic', ['topic' => $topic]);
    }

    public function edited($id, Request $request)
    {
        $topic = Topic::find($id);
        $topic->name = $request->get('name');
        $topic->save();
        return redirect('/admin/topics');
    }

    public function deleted($id)
    {
        $topic = Topic::find($id);
        $notes = $topic->notes;
        $noteController = new NoteController();
        for ($i = 0; $i < count($notes); $i++) {
            $noteController->deleted($notes[$i]->id);
        }
        $topic->delete();
        return redirect('/admin/topics');
    }
}
