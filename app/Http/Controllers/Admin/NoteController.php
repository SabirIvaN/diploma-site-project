<?php

namespace App\Http\Controllers\Admin;

use App\Note;
use App\Topic;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class NoteController extends Controller
{
    public function index()
    {
        $notes = Note::all();
        return view('admin.notes', ['notes' => $notes]);
    }

    public function add()
    {
        $topics = Topic::all();
        return view('admin.adding.note', ['topics' => $topics]);
    }

    public function added(Request $request)
    {
        $title = $request->get('title');
        $image = $request->file('image')->getClientOriginalExtension();
        $stringImageReFormat = str_replace(' ', '', $title);
        $imageName = $stringImageReFormat . '.' . $image;
        $imageEncoded = File::get($request->image);
        $description = $request->get('description');
        Storage::disk('local')->put('public/img/' . $imageName, $imageEncoded);
        $content = $request->get('content');
        $topicId = Topic::where('name', '=', $request->get('topic'))->first()->id;
        $author = Auth::user()->name . ' ' . Auth::user()->patronymic . ' ' . Auth::user()->surname;
        $slider = $request->get('slider');
        $topic = Topic::find($topicId);
        $note = new Note();
        $note->title = $title;
        $note->image = $imageName;
        $note->text = $content;
        $note->author = $author;
        $note->slider = $slider;
        if ($topic->notes()->save($note)) {
            return redirect('admin/notes/');
        } else {
            abort(404);
        }
    }

    public function edit($id)
    {
        $note = Note::find($id);
        $topics = Topic::all();
        return view('admin.editing.note', ['note' => $note, 'topics' => $topics]);
    }

    public function edited($id, Request $request)
    {
        $note = Note::find($id);
        $title = $request->get('title');
        if ($request->hasFile('image')) {
            $exists = Storage::disk('local')->exists('public/img/' . $note->image);
            if ($exists) {
                Storage::delete('public/img/' . $note->image);
            }
            $image = $request->file('image')->getClientOriginalExtension();
            $imageName = $note->image;
            $request->image->storeAs('public/img/', $imageName);
        }
        $content = $request->get('content');
        $author = Auth::user()->name . ' ' . Auth::user()->patronymic . ' ' . Auth::user()->surname;
        $slider = $request->get('slider');
        $topicId = Topic::where('name', '=', $request->get('topic'))->first()->id;
        $topic = Topic::find($topicId);
        $note->title = $title;
        $note->text = $content;
        $note->author = $author;
        $note->slider = $slider;
        if ($topic->notes()->save($note)) {
            return redirect('admin/notes');
        } else {
            abort(404);
        }
    }

    public function deleted($id)
    {
        $note = Note::find($id);
        $exists = Storage::disk('local')->exists('public/img/' . $note->image);
        if ($exists) {
            Storage::delete('public/img/' . $note->image);
        }
        $note->comments()->delete();
        Note::destroy($id);
        return redirect('admin/notes');
    }
}
