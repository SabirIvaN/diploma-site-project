<?php

namespace App\Http\Controllers\Admin;

use App\Review;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    public function index()
    {
        $reviews = Review::groupBy('created_at')->orderBy('created_at', 'DESC')->get();
        $dates = [];
        $array = [];
        for ($i = 0; $i < count($reviews); $i++) {
            $dates[] = carbonFormat($reviews[$i]->created_at);
        }
        array_unique($dates);
        foreach ($dates as $date) {
            $array[$date] = [];
            foreach ($reviews as $review) {
                if (carbon($date, $review)) {
                    array_push($array[$date], $review);
                }
            }
        }
        return view('admin.reviews', ['reviews' => $array]);
    }

    public function deleted($id)
    {
        Review::destroy($id);
        return redirect('/admin/reviews/');
    }
}
