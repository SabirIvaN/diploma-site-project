<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Product;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        return view('admin.categories', ['categories' => $categories]);
    }

    public function showAdd()
    {
        return view('admin.adding.category');
    }

    public function added(Request $request)
    {
        $category = new Category();
        $category->name = $request->get('name');
        $category->save();
        return redirect('/admin/categories');
    }

    public function deleted($id)
    {
        $category = Category::find($id);
        $category->products()->detach();
        $category->delete();
        return redirect('/admin/categories');
    }

    public function showEdit($id)
    {
        $category = Category::find($id);
        return view('admin.editing.category', ['category' => $category]);
    }

    public function edited(Request $request, $id)
    {
        $category = Category::find($id);
        $category->name = $request->get('name');
        $category->save();
        return redirect('/admin/categories/');
    }
}
