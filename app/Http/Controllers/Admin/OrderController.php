<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Order;
use App\Mail\CompletionMail;
use App\Mail\CanceledOrderMail;
use App\Mail\BuildingOrderMail;
use App\Mail\RefundApprovedMail;
use App\Mail\RefundDisaprovedMail;
use App\Mail\WaitingDeliverlyMail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    public function index()
    {
        $orders = Order::where('status', '!=', 'завершен')
            ->where('status', '!=', 'отменен')
            ->where('status', '!=', 'рассматривается для возврата')
            ->where('status', '!=', 'одобрен для возврата')
            ->where('status', '!=', 'не одобрен для возврата')->get();
        return view('admin.orders', ['orders' => $orders]);
    }

    public function orderAssembly($id)
    {
        $order = Order::find($id);
        $order->status = 'собирается';
        if ($order->save()) {
            Mail::to($order->user->email)->send(new BuildingOrderMail($order));
            return redirect('/admin/orders');
        } else {
            abort(404);
        }
    }

    public function orderRefund($id)
    {
        $order = Order::find($id);
        $order->status = 'отменен';
        if ($order->save()) {
            Mail::to($order->user->email)->send(new CanceledOrderMail($order));
            return redirect('/admin/orders/');
        }
        abort(404);
    }

    public function deleteOrder($id)
    {
        $order = Order::find($id);
        if($order->items()->delete()) {
            if ($order->delete()) {
                return redirect('/admin/orders');
            }
        }
        abort(404);
    }

    public function add($id)
    {
        $order = Order::find($id);
        if ($order->status == 'собирается') {
            return view('admin.adding.date', ['order' => $order]);
        }
        abort(404);
    }

    public function added(Request $request, $id) {
        $order = Order::find($id);
        if ($order->status == 'собирается') {
            $order->delivery_date = $request->get('delivery-date');
            $order->status = 'ожидает доставки';
            if ($order->save()) {
                Mail::to($order->user->email)->send(new WaitingDeliverlyMail($order));
                return redirect('/admin/orders');
            }
        }
        abort(404);
    }

    public function edit($id)
    {
        $order = Order::find($id);
        if ($order->status == 'ожидает доставки') {
            return view('admin.editing.date', ['order' => $order]);
        }
        abort(404);
    }

    public function edited(Request $request, $id)
    {
        $order = Order::find($id);
        if ($order->status == 'ожидает доставки') {
            $order->delivery_date = $request->get('delivery-date');
            if ($order->save()) {
                Mail::to($order->user->email)->send(new WaitingDeliverlyMail($order));
                return redirect('/admin/orders');
            }
        }
        abort(404);
    }

    public function orderCompleted($id)
    {
        $order = Order::find($id);
        if ($order->status == 'ожидает доставки') {
            $order->status = 'завершен';
            if ($order->save()) {
                Mail::to($order->user->email)->send(new CompletionMail($order));
                return redirect('/admin/orders');
            }
        }
        abort(404);
    }

    public function orderInformation($id)
    {
        $order = Order::find($id);
        return view('admin.order', ['order' => $order]);
    }

    public function indexReturns()
    {
        $orders = Order::where('status', '=', 'рассматривается для возврата')->get();
        return view('admin.returns', ['orders' => $orders]);
    }

    public function returnApprove($id)
    {
        $order = Order::find($id);
        $order->status = 'одобрен для возврата';
        if ($order->save()) {
            Mail::to($order->user->email)->send(new RefundApprovedMail($order));
            return redirect('/admin/orders');
        } else {
            abort(404);
        }
    }

    public function returnDisapprove($id)
    {
        $order = Order::find($id);
        $order->status = 'не одобрен для возврата';
        if ($order->save()) {
            Mail::to($order->user->email)->send(new RefundDisaprovedMail($order));
            return redirect('/admin/returns');
        } else {
            abort(404);
        }
    }

    public function indexHistory()
    {
        $orders = Order::where('status', '=', 'завершен')
            ->orWhere('status', '=', 'отменен')
            ->orWhere('status', '=', 'одобрен для возврата')
            ->orWhere('status', '=', 'не одобрен для возврата')->get();
        return view('admin.history', ['orders' => $orders]);
    }
}
