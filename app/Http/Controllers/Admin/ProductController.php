<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::all();
        return view('admin.products', ['products' => $products]);
    }

    public function showAdd()
    {
        $categories = Category::all();
        return view('admin.adding.product', ['categories' => $categories]);
    }

    public function added(Request $request)
    {
        $name = $request->get('name');
        $price = $request->get('price');
        $categories = $request->get('categories');
        for ($i = 0; $i < count($categories); $i++) {
            $category = Category::where('name', '=', $categories[$i])->first();
            $categoriesId[] = $category['id'];
        }
        $image = $request->file('image')->getClientOriginalExtension();
        $stringImageReFormat = str_replace(' ', '', $name);
        $imageName = $stringImageReFormat . '.' . $image;
        $imageEncoded = File::get($request->image);
        $description = $request->get('description');
        Storage::disk('local')->put('public/img/' . $imageName, $imageEncoded);
        $product = Product::create([
            'name' => $name,
            'description' => $description,
            'image' => $imageName,
            'price' => $price,
        ]);
        if ($product->save()) {
            for ($i = 0; $i < count($categoriesId); $i++) {
                Product::find($product->id)->categories()->attach($categoriesId[$i]);
            }
            return redirect('/admin/products');
        } else {
            abort(404);
        }
    }

    public function showEdit($id)
    {
        $product = Product::find($id);
        $categories = Category::all();
        return view('admin.editing.product', ['product' => $product, 'categories' => $categories]);
    }

    public function edited(Request $request, $id)
    {
        $product = Product::find($id);
        $name = $request->get('name');
        $price = $request->get('price');
        $categories = $request->get('categories');
        for ($i = 0; $i < count($categories); $i++) {
            $category = Category::where('name', '=', $categories[$i])->first();
            $categoriesId[] = $category['id'];
        }
        if ($request->hasFile('image')) {
            $exists = Storage::disk('local')->exists('public/img/' . $product->image);
            if ($exists) {
                Storage::delete('public/img/' . $product->image);
            }
            $image = $request->file('image')->getClientOriginalExtension();
            $imageName = $product->image;
            $request->image->storeAs('public/img/', $imageName);
        }
        $imageName = $product->image;
        $description = $request->get('description');
        $product = $product->update([
            'name' => $name,
            'description' => $description,
            'image' => $imageName,
            'price' => $price,
        ]);
        if ($product) {
            Product::find($id)->categories()->detach();
            for ($i = 0; $i < count($categoriesId); $i++) {
                Product::find($id)->categories()->attach($categoriesId[$i]);
            }
            return redirect('/admin/products');
        } else {
            abort(404);
        }
    }

    public function deleted($id)
    {
        $product = Product::find($id);
        $product->reviews()->delete();
        $exists = Storage::disk('local')->exists('public/img/' . $product->image);
        if ($exists) {
            Storage::delete('public/img/' . $product->image);
        }
        $product->categories()->detach();
        Product::destroy($id);
        return redirect('/admin/products');
    }
}
