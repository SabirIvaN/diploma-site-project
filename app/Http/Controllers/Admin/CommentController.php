<?php

namespace App\Http\Controllers\Admin;

use App\Comment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function index()
    {
        $comments = Comment::groupBy('created_at')->orderBy('created_at', 'DESC')->get();
        $dates = [];
        $array = [];
        for ($i = 0; $i < count($comments); $i++) {
            $dates[] = carbonFormat($comments[$i]->created_at);
        }
        array_unique($dates);
        foreach ($dates as $date) {
            $array[$date] = [];
            foreach ($comments as $comment) {
                if (carbon($date, $comment)) {
                    array_push($array[$date], $comment);
                }
            }
        }
        return view('admin.comments', ['comments' => $array]);
    }

    public function deleted($id)
    {
        Comment::destroy($id);
        return redirect('/admin/comments/');
    }
}
