<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Notifications\VerifyRemoveUser;
use App\Notifications\InvoicePaid;
use Illuminate\Notifications\Notifiable;
use Notification;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Mail\RemoteMail;
use App\User;
use Str;

class DeleteController extends Controller
{
    public function send($id)
    {
        $user = User::find($id);
        if ($user->save()) {
            Notification::route('mail', $user->email)->notify(new VerifyRemoveUser($user->id));
            return redirect('/home');
        } else {
            abort(404);
        }
    }

    public function delete($id)
    {
        $user = User::find($id);
        Mail::to($user->email)->send(new RemoteMail());
        $user->delete();
        return redirect('/home');
    }
}
