<?php

namespace App\Http\Controllers\User;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Mail\PasswordMail;
use App\User;

class PasswordController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit(User $user)
    {
        $user = Auth::user();
        return view('user.password.edit', ['user' => $user]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        $user = User::find($id);
        $oldPassword = $request->get('old-password');
        $password = Hash::make($request->get('password'));
        if (Hash::check($oldPassword, Auth::user()->password)) {
            $user->password = $password;
        } else {
            return redirect('/home/password/edit/' . $id);
        }
        if ($user->update()) {
            Mail::to($user->email)->send(new PasswordMail($request->get('password')));
            return redirect('/home');
        } else {
            abort(404);
        }
    }
}
