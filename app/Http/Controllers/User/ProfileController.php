<?php

namespace App\Http\Controllers\User;

use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Notification;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Notifications\VerifyChangesProfile;
use App\Mail\ProfileMail;
use App\User;
use Str;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit(User $user)
    {
        $user = Auth::user();
        return view('user.profile.edit', ['user' => $user]);
    }

    public function save(Request $request, $id)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        ]);
        $user = User::find($id);
        $newData['name'] = $request->get('name');
        $newData['surname'] = $request->get('surname');
        $newData['patronymic'] = $request->get('patronymic');
        $newData['sex'] = $request->get('sex');
        $newData['email'] = $request->get('email');
        $newData['phone'] = $request->get('phone');
        $newData['address'] = $request->get('address');
        $user->new_data = json_encode($newData);
        $user->changes_token = Str::random(20);
        if ($user->save()) {
            Notification::route('mail', $newData['email'])->notify(new VerifyChangesProfile($user->changes_token, $user->id));
            return redirect('/home');
        } else {
            abort(404);
        }
    }

    public function update($id, $changesToken)
    {
        $user = User::find($id);
        $newData = json_decode($user->new_data, TRUE);
        $user->name = $newData['name'];
        $user->surname = $newData['surname'];
        $user->patronymic = $newData['patronymic'];
        $user->sex = $newData['sex'];
        $user->email = $newData['email'];
        $user->phone = $newData['phone'];
        $user->address = $newData['address'];
        $user->new_data = null;
        $user->changes_token = null;
        if ($user->update()) {
            Mail::to($user->email)->send(new ProfileMail([
                'name' => $user->name,
                'surname' => $user->surname,
                'patronymic' => $user->patronymic,
                'sex' => $user->sex,
                'email' => $user->email,
                'phone' => $user->phone,
                'address' => $user->address,
            ]));
            return redirect('/home');
        } else {
            abort(404);
        }
    }
}
