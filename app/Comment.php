<?php

namespace App;

use App\Note;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'comments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name', 'email', 'content',
    ];

    public function note()
    {
        return $this->belongsTo(Note::class);
    }
}
