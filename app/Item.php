<?php

namespace App;

use App\Order;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'items';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'price', 'quantity', 'total_price',
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
