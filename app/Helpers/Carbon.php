<?php

use Carbon\Carbon;

function carbon($date, $object) {
    return $date == Carbon::parse($object->created_at)->format('Y-m-d') ? true : false;
}

function carbonFormat($date) {
    return Carbon::parse($date)->format('Y-m-d');
}
