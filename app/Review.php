<?php

namespace App;

use App\Product;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'reviews';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name', 'email', 'evaluation', 'content',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
