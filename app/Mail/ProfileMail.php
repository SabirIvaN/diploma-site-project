<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ProfileMail extends Mailable
{
    use Queueable, SerializesModels;

    public $content = [
        'name', 'surname', 'patronymic', 'sex', 'email', 'phone', 'address',
    ];

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($content)
    {
        $this->content['name'] = $content['name'];
        $this->content['surname'] = $content['surname'];
        $this->content['patronymic'] = $content['patronymic'];
        $this->content['sex'] = $content['sex'];
        $this->content['email'] = $content['email'];
        $this->content['phone'] = $content['phone'];
        $this->content['address'] = $content['address'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Изменен профиль')
            ->markdown('vendor.notifications.profile');
    }
}
