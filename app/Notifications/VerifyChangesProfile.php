<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class VerifyChangesProfile extends Notification
{
    use Queueable;

    public $changesToken;
    public $id;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($changesToken, $id)
    {
        $this->id = User::find($id)->id;
        $this->changesToken = User::find($id)->changes_token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Подтвердите внесенные изменения')
                    ->line('Пожалуйста нажмите на кнопку для подтверждения внесенных изменений.')
                    ->action('Подтвердить изменения', url('/home/user/profile/edit/update/' . $this->id . '/' . $this->changesToken))
                    ->line('Если Вы не вносили изменений в свой профиль, никаких дальнейших действий не требуется.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
